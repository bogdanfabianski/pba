#include "encodeAscii.h"

const char all_ascii[512] = ALL_ASCII;

void encodeAscii(uint8_t* inbuf, char* outstr, uint16_t len)
{
	uint16_t i;
	for(i=0; i<len; i++)
	{
		memcpy((char*)(outstr+2*i), (char*) (all_ascii+2*(*(inbuf+i))),2);
	}
	*(outstr+2*len) = 0x00;
}

uint32_t PROT_MakeIDFromHexString(char* pcText, uint8_t size)
{
	int8_t  index = 0;
	uint32_t uValue = 0x00;
	char oneChar = 	*(pcText+0);
	if (size>8){size=8;}
	for(index=0; index<size; index++)
	{	  
	    uValue<<=4;
	    oneChar = 	*(pcText+index);
		if(oneChar >= 0x30 && oneChar <= 0x39){	  //0-9
		  oneChar=oneChar-0x30;	
		} else if(oneChar >= 0x41 && oneChar <= 0x46){	//A-F
		  oneChar=oneChar-0x37;	
		} else if(oneChar >= 0x61 && oneChar <= 0x66){	//a-f
		  oneChar=oneChar-0x57;	
		} else {(oneChar = 0x00);}
	    uValue |= (oneChar & 0x0F);
	}
	return(uValue);
}

uint8_t decodeAscii(char* inbuf, uint8_t* outbuf, uint8_t size){
	uint8_t var;
	uint8_t index = 0;
	uint8_t index_hex = 0;
	for(index_hex=0; index_hex<size; index_hex+=2){
		var =	PROT_MakeIDFromHexString(inbuf+index_hex, 2);
		*(outbuf+index) = var;
	  index+=1;
	}
	*(outbuf+index)= 0;
	return(index);
}
