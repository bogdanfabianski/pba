/**
*	@author Dominik Luczak, Bogdan Fabianski
*	@date 2018-09-25
*	@brief Parameters
*/
#include "parameters/parameter.h"
#include "parameters/variable.h"
#include "stm32h7xx_hal.h"


	
//#pragma NOINIT
__attribute__((section(".backup"), zero_init)) volatile uint8_t load_defaults;

parameters_t parameters;     //__attribute__((section(".noinit")));

/**
 * Get size of parameter list
 * @return size of parameter list
 */
 
 uint8_t parameter_get_save_status(void){
	return(parameters.last_save_status);
}
 
uint8_t parameter_need_restore(void){
	if(load_defaults == MAGIC_NUMBER_LOAD_DEFAULTS){return(1);}else{return(0);}
}

void parameter_set_restore(void){
	//PWR->CR1 |= PWR_CR1_DBP;
	load_defaults = MAGIC_NUMBER_LOAD_DEFAULTS;
	//PWR->CR1 &= ~(PWR_CR1_DBP);
}

void parameter_reset_restore(void){
	//	PWR->CR1 |= PWR_CR1_DBP;
	  load_defaults = 0x00;
	//	PWR->CR1 &= ~(PWR_CR1_DBP);
}
 
static uint32_t parameter_get_size_of_list(void);


parameter_t* parameter_find_by_id(uint32_t id){
	
	parameter_t* pParameter=0;	
	variable_t* pStart = (variable_t*) parameter_get_fisrt();
	uint32_t end_address = (uint32_t) parameter_get_fisrt();
	end_address += parameter_get_size_of_list();
	
	pParameter = (parameter_t*) variable_find_by_id( id, pStart, end_address);
	
	return pParameter;
}

parameter_t* parameter_set_value_by_id(uint32_t id, char* value_string)
{
	parameter_t* pParameter=0;	
	pParameter = parameter_find_by_id(id);
	
	if(pParameter)
	variable_set_value( (variable_t*) pParameter, value_string);
	
	return pParameter;
}

parameter_t* parameter_get_fisrt(void)
{
	return (parameter_t*) &parameters.list_s;
}


parameter_t* parameter_get_next_parameter(parameter_t* pParameter)
{	
	uint32_t end_address = (uint32_t) parameter_get_fisrt();
	end_address += parameter_get_size_of_list();
	parameter_t* pParameter_next=0;
	
	pParameter_next = (parameter_t*)variable_get_next_variable((variable_t*) pParameter);	
	if((uint32_t)pParameter_next >= end_address ) pParameter_next=NULL; // NULL - out of memory range
	
	return pParameter_next;
}


//Private function

static uint32_t parameter_get_size_of_list(void)
{
	return sizeof(parameters.list_s);
}



