/**
*	@author Dominik Luczak, Bogdan Fabianski
*	@date 2018-10-20
*	@brief Parameters non-volatile memory
*/

#include "stm32h7xx_hal.h"
#include "parameters/parameter-nvm.h"
//Software
#include "parameters/parameter.h"
#include "parameters/variable.h"
#include "parameters/flash.h"
#include "commands.h"
//Hardware

extern CRC_HandleTypeDef hcrc;

uint8_t parameter_nvm_save(void)
{
	uint8_t  result=0xFF;
	RTT_LOG("Saving parameters...");
  //HAL_Delay(10);
	if(memcmp (&parameters, (void *)FLASH_USER_START_ADDR, sizeof(parameters_t)-8) !=0 )
	{	
		HAL_NVIC_DisableIRQ(SPI4_IRQn);
		HAL_NVIC_DisableIRQ(SPI5_IRQn);
		HAL_NVIC_DisableIRQ(DMA1_Stream0_IRQn);
		//HAL_NVIC_DisableIRQ(DMA1_Stream1_IRQn);
		//HAL_NVIC_DisableIRQ(SPI1_IRQn);
	  //HAL_SPI_Abort_IT(&hspi1);
	  HAL_Delay(10);
		//result = 0xFF;
		//Compute CRC32 code from parameters data	
		parameters.crc32 = HAL_CRC_Calculate(&hcrc, (uint32_t*) &parameters, sizeof(parameters_t)-8);
		
		//Write data from RAM to flash
		result = flash_write((uint32_t*)&parameters, sizeof(parameters_t));
		if(result==0x00){RTT_LOG("OK");}else{RTT_LOG("fail");}
		HAL_Delay(10);
		NVIC_SystemReset();
    //HAL_NVIC_EnableIRQ(SPI1_IRQn);		
		//error_handling_spi_reset();
	}else{
		RTT_LOG("No changes in data parameters");
	}		
	
	//else {
	//	result = 0xFF;
	//}
	parameters.last_save_status = result;
	//osDelay(50);

	return result;
	
}

void parameter_set_last_save_status(uint8_t status){
	parameters.last_save_status = status;
}

void parameter_nvm_load(void)
{
	parameters_t* pParameters_NVM = (parameters_t*)FLASH_USER_START_ADDR;
	RTT_LOG("Reading parameters...");
	uint32_t nvm_crc32 = HAL_CRC_Calculate(&hcrc, (uint32_t*) pParameters_NVM, sizeof(parameters_t)-8);
	
	if(pParameters_NVM->struct_version==parameters.struct_version && pParameters_NVM->size==parameters.size && pParameters_NVM->count==parameters.count && pParameters_NVM->crc32==nvm_crc32)
	{		
		//Read each parameter one by one
		parameter_t* pParameter_element_RAM = parameter_get_fisrt(); //First element in list
		parameter_t* pParameter_element_NVM = (parameter_t*)&pParameters_NVM->list_s; //First element in list
	
		uint8_t parameter_element_size	= variable_size_of((variable_t*)pParameter_element_RAM); 	//Sizeof parameter element
		variable_const_t* 	pConst			= 0; 																											//Pointer address to constant part of variable
		
		while(pParameter_element_RAM!=NULL && parameter_element_size!=0)
		{
			pConst	= pParameter_element_RAM->settings_s.pConst_s;	//copy proper pointer of const
			memcpy(pParameter_element_RAM, pParameter_element_NVM, parameter_element_size); //copy data all data of single variable from NVM (with bad pointer of const)
			pParameter_element_RAM->settings_s.pConst_s	= pConst;		//restore proper pointer of const
			
			pParameter_element_RAM = parameter_get_next_parameter(pParameter_element_RAM); 		//point to next element		
			pParameter_element_NVM = (variable_t*)((uint32_t)pParameter_element_NVM+parameter_element_size);//(parameter_t*)variable_get_next_variable((variable_t*) pParameter_element_NVM); 	//point to next element
			parameter_element_size = variable_size_of((variable_t*)pParameter_element_RAM); 				
		}	
		
		RTT_LOG("Success.");
	}
	//Incompatible structure
	else
	{
		RTT_LOG("Parameters structure incompatible. Operation failed.");
	}
}
