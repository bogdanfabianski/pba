/**
*	@author Dominik Luczak
*	@date 2018-09-25
*	@brief Parameters
*/
#include "parameters/parameter-setup-const.h"
#include "parameters/parameter.h"
#include "parameters/parameter-type.h"
#include "parameters/parameter-enum.h"
#include "parameters/access-level.h"

#define parameter_initialization(identifier, parameter_name, parameter_id, parameter_value, parameter_type, parameter_access_level, parameter_name_text, parameter_description_text) \
	parameters.count++;	\
	static const variable_const_t _setup_parameter_struct_ ## identifier ## _ = { parameter_id, parameter_type, parameter_name_text, parameter_description_text }; \
	parameter_name.settings_s.pConst_s = (variable_const_t* )&_setup_parameter_struct_ ## identifier ## _ ; \
	parameter_name.value 	= parameter_value; \
	parameter_name.settings_s.var_s.access_level 	= parameter_access_level;


void parameter_setup(void)
{
	//Parameters initialization	
	parameters.struct_version 	= 0x00003;	
	parameters.size 			= sizeof(parameters_t);
	parameters.count 			= 0;
	parameters.refresh 		= 1;
	//parameters.load_defaults = 0;
	parameters.last_save_status = 0xee;
	

	//PERIPHERIAL INTERFACE
	parameter_initialization(PERIPHERIAL_SELECT_INTERFACE, parameters.list_s.peripheral_s.interface,		0x1001, 	peripheral_interface_disable_e,	variable_select_e, level_develop_e,	"interface.type", 				"{\\\"d\\\":\\\"Interface type\\\",\\\"i\\\":[\\\"Disabled\\\",\\\"ETHERNET\\\",\\\"CAN\\\",\\\"RS485\\\",\\\"RS232\\\"]}" );	
	//PERIPHERIAL protocol
	parameter_initialization(PERIPHERAL_PROTOCOL, parameters.list_s.peripheral_s.protocol,   0x1006, DEFAULT_PROTOCOL, variable_select_e, level_develop_e, "serial.protocol", "{\\\"d\\\":\\\"Serial protocol\\\",\\\"i\\\":[\\\"Raw\\\",\\\"Modbus\\\",\\\"Profibus\\\"]}");
  
	parameter_initialization(PERIPHERAL_ETHERNET_OPERATION_MODE,parameters.list_s.peripheral_s.ethernet_s.operation_mode,	0x1041, PA_PB_ETHERNET_OPERATION_MODE,		variable_select_e, level_develop_e,"PAPB.ethernet.operation_mode","PA,PB ethernet operation mode");



	parameter_initialization(PERIPHERAL_PA_MAC_A1, 		parameters.list_s.peripheral_s.ethernet_s.macPA.A1,		0x1020, 	PA_MAC_ADDR0,	variable_uint8_e, level_develop_e,	"peripheral.ethernet.PAMAC.A1", "MAC");
	parameter_initialization(PERIPHERAL_PA_MAC_A2, 		parameters.list_s.peripheral_s.ethernet_s.macPA.A2,		0x1021, 	PA_MAC_ADDR1,	variable_uint8_e, level_develop_e,	"peripheral.ethernet.PAMAC.A2", "MAC");
	parameter_initialization(PERIPHERAL_PA_MAC_A3, 		parameters.list_s.peripheral_s.ethernet_s.macPA.A3,		0x1022, 	PA_MAC_ADDR2,	variable_uint8_e, level_develop_e,	"peripheral.ethernet.PAMAC.A3", "MAC");
	parameter_initialization(PERIPHERAL_PA_MAC_A4, 		parameters.list_s.peripheral_s.ethernet_s.macPA.A4,		0x1023, 	PA_MAC_ADDR3,	variable_uint8_e, level_develop_e,	"peripheral.ethernet.PAMAC.A4", "MAC");
	parameter_initialization(PERIPHERAL_PA_MAC_A5, 		parameters.list_s.peripheral_s.ethernet_s.macPA.A5,		0x1024, 	PA_MAC_ADDR4,	variable_uint8_e, level_develop_e,	"peripheral.ethernet.PAMAC.A5", "MAC");
	parameter_initialization(PERIPHERAL_PA_MAC_A6, 		parameters.list_s.peripheral_s.ethernet_s.macPA.A6,		0x1025, 	PA_MAC_ADDR5,	variable_uint8_e, level_develop_e,	"peripheral.ethernet.PAMAC.A6", "MAC");
	
	/*
	//IP address - peripheral
	parameter_initialization(PERIPHERAL_IP_A1, 		parameters.list_s.peripheral_s.ethernet_s.ip.A1,		0x102C, 	PB_IP_ADDR0,	variable_uint8_e, level_develop_e,	"PB.IP.A1", 					"IP PB");
	parameter_initialization(PERIPHERAL_IP_A2, 		parameters.list_s.peripheral_s.ethernet_s.ip.A2,		0x102D, 	PB_IP_ADDR1,	variable_uint8_e, level_develop_e,	"PB.IP.A2", 			"IP PB"		);
	parameter_initialization(PERIPHERAL_IP_A3, 		parameters.list_s.peripheral_s.ethernet_s.ip.A3,		0x102E, 	PB_IP_ADDR2,	  variable_uint8_e, level_develop_e,	"PB.IP.A3", 		"IP PB"			);
	parameter_initialization(PERIPHERAL_IP_A4, 		parameters.list_s.peripheral_s.ethernet_s.ip.A4,		0x102F, 	PB_IP_ADDR3,		variable_uint8_e, level_develop_e,	"PB.IP.A4", 		"IP PB"			);

	//Netmask address - peripheral
	parameter_initialization(PERIPHERAL_NETMASK_A1, 		parameters.list_s.peripheral_s.ethernet_s.netmask.A1,		0x1030, 	PB_NETMASK_ADDR0,	variable_uint8_e, level_develop_e,	"PB.NM.A1", 					"NM PB");
	parameter_initialization(PERIPHERAL_NETMASK_A2, 		parameters.list_s.peripheral_s.ethernet_s.netmask.A2,		0x1031, 	PB_NETMASK_ADDR1,	variable_uint8_e, level_develop_e,		"PB.NM.A2", 					"NM PB");
	parameter_initialization(PERIPHERAL_NETMASK_A3, 		parameters.list_s.peripheral_s.ethernet_s.netmask.A3,		0x1032, 	PB_NETMASK_ADDR2,	variable_uint8_e, level_develop_e,		"PB.NM.A3", 					"NM PB");
	parameter_initialization(PERIPHERAL_NETMASK_A4, 		parameters.list_s.peripheral_s.ethernet_s.netmask.A4,		0x1033, 	PB_NETMASK_ADDR3,		variable_uint8_e, level_develop_e,		"PB.NM.A4", 					"NM PB");

	//Gateway address - peripheral
	parameter_initialization(PERIPHERAL_GATEWAY_A1, 		parameters.list_s.peripheral_s.ethernet_s.gateway.A1,		0x1034, 	PB_GW_ADDR0,	variable_uint8_e, level_develop_e,	"PB.GW.A1", 					"GW PB");
	parameter_initialization(PERIPHERAL_GATEWAY_A2, 		parameters.list_s.peripheral_s.ethernet_s.gateway.A2,		0x1035, 	PB_GW_ADDR1,	variable_uint8_e, level_develop_e,	"PB.GW.A2", 					"GW PB");
	parameter_initialization(PERIPHERAL_GATEWAY_A3, 		parameters.list_s.peripheral_s.ethernet_s.gateway.A3,		0x1036, 	PB_GW_ADDR2,	  variable_uint8_e, level_develop_e,"PB.GW.A3", 					"GW PB");
	parameter_initialization(PERIPHERAL_GATEWAY_A4, 		parameters.list_s.peripheral_s.ethernet_s.gateway.A4,		0x1037, 	PB_GW_ADDR3,		variable_uint8_e, level_develop_e,"PB.GW.A4", 					"GW PB");

  //ethernet protocol
	//parameter_initialization(PERIPHERAL_ETHERNET_PROTOCOL, 		  parameters.list_s.peripheral_s.ethernet_s.ethernet_protocol,		0x1040, 	PA_PB_ETHERNET_PROTOCOL,		variable_select_e, level_develop_e,"PAPB.ethernet.protocol", 					"PA,PB ethernet protocol");
*/ 


	//Dummy
	parameter_initialization(DUMMY, 		parameters.list_s.dummy,		0xFFFF, 	0,		variable_uint32_e, level_develop_e,	"dummy", 					"Dummy parameter only for debug.");

}
