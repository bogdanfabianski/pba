/**
*	@author Dominik Luczak, Bogdan Fabianski
*	@date 2018-08-22
*	@brief Firmware version
*/

#include "parameters/firmware-version.h"

const char* firmware_version = "312201";
const char* firmware_build_time =  __DATE__ ":" __TIME__ ;
