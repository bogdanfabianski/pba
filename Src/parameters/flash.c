/**
*	@author Dominik Luczak, Bogdan Fabianski
*	@date 2018-10-20
*	@brief Flash memory
*/


#include "stm32h7xx_hal.h"
//Hardware
#include "parameters/flash.h"
#include "main.h"
#include "led.h"

static uint32_t GetSector(uint32_t Address);

__IO uint8_t flag_flash_op_completed = 0;
__IO uint8_t flag_flash_op_error = 0;

extern FLASH_ProcessTypeDef pFlash;

void HAL_FLASH_EndOfOperationCallback(uint32_t ReturnValue)
{
	CLEAR_BIT(FLASH->CR1, FLASH_CR_PG);
  __HAL_UNLOCK(&pFlash);
		flag_flash_op_completed = 1;
//	static BaseType_t xHigherPriorityTaskWoken = pdFALSE;
//	xSemaphoreGiveFromISR(semaphoreFlashOperationSync, NULL);//&xHigherPriorityTaskWoken);
//if(xHigherPriorityTaskWoken == pdTRUE) {
//	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );//}	
}

void HAL_FLASH_OperationErrorCallback(uint32_t ReturnValue)
{
    RTT_LOG("Flash operation error");
	  flag_flash_op_error = HAL_FLASH_GetError();
}

static void flash_return_routine(void){
		HAL_NVIC_DisableIRQ(FLASH_IRQn);
	__HAL_FLASH_CLEAR_FLAG_BANK1(FLASH_FLAG_EOP_BANK1 | FLASH_FLAG_ALL_ERRORS_BANK1); 
	__HAL_FLASH_DISABLE_IT_BANK1(FLASH_IT_EOP_BANK1    | FLASH_IT_WRPERR_BANK1 | FLASH_IT_PGSERR_BANK1 | \
                                FLASH_IT_STRBERR_BANK1 | FLASH_IT_INCERR_BANK1 | FLASH_IT_OPERR_BANK1); 
	
	CLEAR_BIT(FLASH->CR1, FLASH_CR_PG);

	HAL_FLASH_Lock();
	__HAL_UNLOCK(&pFlash);
  //HAL_Delay(100);
	#ifndef CHECK_SYSTEM_LOAD	
	 DSIG_RGB_B_GREEN(0);	
	#endif
  DSIG_RGB_B_RED(0); DSIG_RGB_B_BLUE(0);

}


uint8_t flash_write(uint32_t* pData, uint32_t size)
{
	flag_flash_op_completed = 0;
	flag_flash_op_error = 0x00;
	uint32_t FirstSector = 0, NbOfSectors = 0, Address = 0;
//	uint32_t SectorError = 0;
	uint32_t ret = 0x01;
	uint8_t delay = 0;
	//Variable used for Erase procedure
	static FLASH_EraseInitTypeDef EraseInitStruct;
	
	uint32_t num_of_flashword = (size/32) + ((size%32)?1:0); //Flash word - 256 bits => 32 bytes
	
	//Exit if overflows available memory space of 128 Kbytes
	if(size > 128*1024) 
	{
		DSIG_RGB_B_RED(1);
		flash_return_routine();
		return(0x10);
	}
	
	// Unlock the Flash to enable the flash control register access 
  HAL_FLASH_Unlock();
    
  // Erase the user Flash
  // Get the 1st sector to erase 
  FirstSector = GetSector(FLASH_USER_START_ADDR);
  // Get the number of sector to erase from 1st sector
  NbOfSectors = GetSector(FLASH_USER_END_ADDR) - FirstSector + 1;

  // Fill EraseInit structure
  EraseInitStruct.TypeErase 	 = FLASH_TYPEERASE_SECTORS;
  EraseInitStruct.VoltageRange = FLASH_VOLTAGE_RANGE_3;
  EraseInitStruct.Banks        = FLASH_BANK_1;	
  EraseInitStruct.Sector 			 = FirstSector;
  EraseInitStruct.NbSectors    = NbOfSectors;
  
  // Note: If an erase operation in Flash memory also concerns data in the data or instruction cache,
  //   you have to make sure that these data are rewritten before they are accessed during code
  //   execution. If this cannot be done safely, it is recommended to flush the caches by setting the
  //   DCRST and ICRST bits in the FLASH_CR register. 
	RTT_LOG("flash erase...");
	flag_flash_op_completed = 0;
	HAL_NVIC_SetPriority(FLASH_IRQn, 7, 0);
  HAL_NVIC_EnableIRQ(FLASH_IRQn);	
	
	#ifndef CHECK_SYSTEM_LOAD	
		DSIG_RGB_B_GREEN(1);
	#endif
	DSIG_RGB_B_RED(1); DSIG_RGB_B_BLUE(1); 
	
	
	if(HAL_FLASHEx_Erase_IT(&EraseInitStruct) != HAL_OK)
  { 
    //  Error occurred while sector erase. 
    //  User can add here some code to deal with this error. 
    //  SectorError will contain the faulty sector and then to know the code error on this sector,
    //  user can call function 'HAL_FLASH_GetError()'    
    //while (1)
    //{
			RTT_LOG("error");
			DSIG_RGB_B_RED(1);
		  flash_return_routine();
		  return(0x11);
		//	NbOfSectors++;
    //}
  } 
//	while (xSemaphoreTake( semaphoreFlashOperationSync, ( TickType_t ) 100 ) != pdTRUE){
	//	osDelay(100);
//	}
	delay=0;
	RTT_LOG("waiting for flash erase...");
	HAL_Delay(200);
	
	while(flag_flash_op_completed==0x00 && delay<20){
		delay++;
		HAL_Delay(20);
	}
	if(delay>=20){
		 flash_return_routine();
	return(0x12);
	}
  // Program the user Flash area word by word (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) 
  Address = FLASH_USER_START_ADDR;

	while(num_of_flashword-- > 0)
	{
		if (Address < FLASH_USER_END_ADDR)
		{
			RTT_LOG("programing  data at %x, val: %d", Address, *pData);
			CLEAR_BIT(FLASH->SR1, FLASH_SR_INCERR);
	    flag_flash_op_completed = 0;
		  ret = HAL_FLASH_Program_IT(FLASH_TYPEPROGRAM_FLASHWORD, Address, (uint64_t)(pData));
		//	while (xSemaphoreTake( semaphoreFlashOperationSync, ( TickType_t ) 100 ) != pdTRUE){
		 //    osDelay(100);
	   // }
			delay=0;
			DSIG_RGB_B_RED_TOGGLE; DSIG_RGB_B_BLUE_TOGGLE; 
				#ifndef CHECK_SYSTEM_LOAD	
			DSIG_RGB_B_GREEN_TOGGLE;
			#endif
			HAL_Delay(50);
	    while(flag_flash_op_completed==0x00 && delay<20){
		  delay++;
		  HAL_Delay(20);
	    }
			if(delay>=20){
					flash_return_routine();
			return(0x13);
			}
			
			//wait for sync from IT
			if(ret == HAL_OK)
			{
				Address = Address + 32; //increment for the next Flash word - 256 bits => 32 bytes
				pData = (uint32_t*)((uint32_t)pData+32);
				RTT_LOG("done");
			}
			else
			{ 
				// Error occurred while writing data in Flash memory. User can add here some code to deal with this error
				//while (1)
				//{
					DSIG_RGB_B_RED(1);
				  RTT_LOG("error (code: %x)",ret);
				  flash_return_routine();
				  return(0x14);

				///}
			}
		}else{
			DSIG_RGB_B_RED(1);
			RTT_LOG("out of user range");
			flash_return_routine();
			return(0x15);
		}
	}

  // Lock the Flash to disable the flash control register access (recommended to protect the FLASH memory against possible unwanted operation) 
 
flash_return_routine();
	return (flag_flash_op_error);
}

/**
  * @brief  Gets the sector of a given address
  * @param  Address Address of the FLASH Memory
  * @retval The sector of a given address
  */
uint32_t GetSector(uint32_t Address)
{
  uint32_t sector = 0;
  
  if(((Address < ADDR_FLASH_SECTOR_1_BANK1) && (Address >= ADDR_FLASH_SECTOR_0_BANK1)) || \
     ((Address < ADDR_FLASH_SECTOR_1_BANK2) && (Address >= ADDR_FLASH_SECTOR_0_BANK2)))    
  {
    sector = FLASH_SECTOR_0;  
  }
  else if(((Address < ADDR_FLASH_SECTOR_2_BANK1) && (Address >= ADDR_FLASH_SECTOR_1_BANK1)) || \
          ((Address < ADDR_FLASH_SECTOR_2_BANK2) && (Address >= ADDR_FLASH_SECTOR_1_BANK2)))    
  {
    sector = FLASH_SECTOR_1;  
  }
  else if(((Address < ADDR_FLASH_SECTOR_3_BANK1) && (Address >= ADDR_FLASH_SECTOR_2_BANK1)) || \
          ((Address < ADDR_FLASH_SECTOR_3_BANK2) && (Address >= ADDR_FLASH_SECTOR_2_BANK2)))    
  {
    sector = FLASH_SECTOR_2;  
  }
  else if(((Address < ADDR_FLASH_SECTOR_4_BANK1) && (Address >= ADDR_FLASH_SECTOR_3_BANK1)) || \
          ((Address < ADDR_FLASH_SECTOR_4_BANK2) && (Address >= ADDR_FLASH_SECTOR_3_BANK2)))    
  {
    sector = FLASH_SECTOR_3;  
  }
  else if(((Address < ADDR_FLASH_SECTOR_5_BANK1) && (Address >= ADDR_FLASH_SECTOR_4_BANK1)) || \
          ((Address < ADDR_FLASH_SECTOR_5_BANK2) && (Address >= ADDR_FLASH_SECTOR_4_BANK2)))    
  {
    sector = FLASH_SECTOR_4;  
  }
  else if(((Address < ADDR_FLASH_SECTOR_6_BANK1) && (Address >= ADDR_FLASH_SECTOR_5_BANK1)) || \
          ((Address < ADDR_FLASH_SECTOR_6_BANK2) && (Address >= ADDR_FLASH_SECTOR_5_BANK2)))    
  {
    sector = FLASH_SECTOR_5;  
  }
  else if(((Address < ADDR_FLASH_SECTOR_7_BANK1) && (Address >= ADDR_FLASH_SECTOR_6_BANK1)) || \
          ((Address < ADDR_FLASH_SECTOR_7_BANK2) && (Address >= ADDR_FLASH_SECTOR_6_BANK2)))    
  {
    sector = FLASH_SECTOR_6;  
  }
  else if(((Address < ADDR_FLASH_SECTOR_0_BANK2) && (Address >= ADDR_FLASH_SECTOR_7_BANK1)) || \
          ((Address < FLASH_END_ADDR) && (Address >= ADDR_FLASH_SECTOR_7_BANK2)))
  {
     sector = FLASH_SECTOR_7;  
  }
  else
  {
    sector = FLASH_SECTOR_7;
  }

  return sector;
}

