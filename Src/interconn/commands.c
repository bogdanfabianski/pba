/**
  ******************************************************************************
  * @file           : commands.c
  * @brief          : Source file "c" for SPI commands
  ******************************************************************************
					PP SCADVANCE TEAM C@2018 (B.Fabianski)
					created: 30.11.2018
					v1.0
  ******************************************************************************
**/

#include "commands.h"
#include "crc_routines.h"
#include "stm32h753xx.h"
#include "stm32h7xx_hal.h"
#include "encodeAscii.h"

#include "ip_parameters.h"
#include "ethernet_parameters.h"

#include "main.h"
#include "parameters/parameter-setup-const.h"
#include "parameters/parameter-type.h"
#include "led.h"


#ifndef USE_SPI_CMD_TEST_B

__IO uint8_t cmd_string[CMD_DATA_MAX_SIZE/2+1];
__IO char cmd_string_ascii[CMD_DATA_MAX_SIZE+1];
uint8_t cmd_tx_delay = 0;

extern SPI_HandleTypeDef hspi5;
extern CRC_HandleTypeDef crcHandle;

protocol_selection_t actual_selected_protocol= (protocol_selection_t)DEFAULT_PROTOCOL;

extern TIM_HandleTypeDef htim12;

extern __IO uint8_t usart1_rx_restore;

extern parameters_t parameters;

ethernet_parameters_t* ethernet_params;

extern uint8_t reset_indicatior;

extern __IO uint8_t parameters_save_flag;


extern __IO uint64_t unix_time_us;

uint64_t val64;



__attribute__((section(".backup"), zero_init)) volatile uint8_t soft_reset;

uint8_t is_soft_reset(void){
	if(soft_reset == SOFT_RESET_MAGIC_NUMBER){return(1);}else{return(0);}
}

void soft_reset_clear(void){
	soft_reset = 0x00;
}


void command_process(char* command_string){
	
	uint16_t len = strlen(command_string);
	if(len<=CMD_DATA_MAX_SIZE){

		decodeAscii(command_string, (uint8_t*)cmd_string, len);
		switch(cmd_string[0]){
			case 0:
					switch(cmd_string[1]){
			    case 1:
				#ifndef PPM_STANDALONE_TEST
			    RTT_LOG("\n\n### COMMAND SYSTEM RESET ###\n");
					soft_reset = 0x00;
			  	NVIC_SystemReset();
			  #endif
					break;
					case 0x11: //PB
						RTT_LOG("\n\n### COMMAND SYSTEM SOFT RESET ###\n");
						soft_reset = SOFT_RESET_MAGIC_NUMBER;
					  NVIC_SystemReset();
					break;
				}
			case 1: //settings
					switch(cmd_string[1]){
						case 1: //usart settings plus update         
							RTT_LOG("###UART param received###\n");
						  parameters_save_flag = 0x01;
						
						break;	
							
						case 0x10: //interface/protocol settings
							
							RTT_LOG("###new I/P param received###\n");
						
						  ip_set_param_from_serial_data((uint8_t*)cmd_string+2);
						  if(actual_selected_protocol!=ip_selected_protocol()){
								actual_selected_protocol = ip_selected_protocol();
								RTT_LOG("selected protocol changed to:%d",actual_selected_protocol);
							}
						
							if(parameters.list_s.peripheral_s.interface.value!=ip_selected_interface()){
							parameters.list_s.peripheral_s.interface.value = ip_selected_interface();
							RTT_LOG("selected interface changed to:%d",parameters.list_s.peripheral_s.interface.value);
							}
								
							switch(parameters.list_s.peripheral_s.interface.value){
								
							case peripheral_interface_rs485_e:
							case peripheral_interface_rs485AB_e:
							case peripheral_interface_rs232_e:
								if(ip_selected_protocol()<=(protocol_selection_t)serial_protocol_DNP3_e){
									parameters.list_s.peripheral_s.protocol.value = ip_selected_protocol();
								} else {
									parameters.list_s.peripheral_s.protocol.value = serial_protocol_raw_e;
								}
							break;
							
							case peripheral_interface_ethernet_e:
								if(ip_selected_protocol()>=(protocol_selection_t)ethernet_protocol_raw_e && ip_selected_protocol()<=(protocol_selection_t)ethernet_protocol_powerlink_e){
								  parameters.list_s.peripheral_s.protocol.value = ip_selected_protocol();
								} else {
									parameters.list_s.peripheral_s.protocol.value = ethernet_protocol_raw_e;
								}
							break;
								
							case peripheral_interface_can_e:
								//nothnig to change 
							break;
								
							default:
									parameters.list_s.peripheral_s.interface.value = peripheral_interface_disable_e;
							break;
						
							}
							actual_selected_protocol = (protocol_selection_t)parameters.list_s.peripheral_s.protocol.value;
						
							
						 
						break;
							
							case 0x20: //ethernet settings 
							RTT_LOG("###Ethernet param received###\n");
							
							
							ethernet_set_param_from_serial_data((uint8_t*)cmd_string+2);
						  ethernet_params = ethernet_get_param_structure();
						
						  parameters.list_s.peripheral_s.ethernet_s.operation_mode.value =  ethernet_params->ethernet_operation_mode;
							
							RTT_LOG("ETH op mode:%d", parameters.list_s.peripheral_s.ethernet_s.operation_mode.value);
							
							parameters.list_s.peripheral_s.ethernet_s.macPA.A1.value =  ethernet_params->macPB[0];
							parameters.list_s.peripheral_s.ethernet_s.macPA.A2.value =  ethernet_params->macPB[1];		
							parameters.list_s.peripheral_s.ethernet_s.macPA.A3.value =  ethernet_params->macPB[2];		
							parameters.list_s.peripheral_s.ethernet_s.macPA.A4.value =  ethernet_params->macPB[3];		
							parameters.list_s.peripheral_s.ethernet_s.macPA.A5.value =  ethernet_params->macPB[4];		
							parameters.list_s.peripheral_s.ethernet_s.macPA.A6.value =  ethernet_params->macPB[5];	

						  break;	
							
							  case 0x30: //timestamp
								val64 = (uint64_t)*((uint32_t*)(cmd_string+2))+ (((uint64_t)(*((uint32_t*)(cmd_string+6))))<<32);
							  HAL_SuspendTick();
								unix_time_us = val64*1000;
							  HAL_ResumeTick();
								RTT_LOG("\n\n###new TIMESTAMP received: %lld###\n", val64);
							break;
							
							default:
							RTT_LOG("Unknown command\n");	
							break;
					}
			break;
			
		}
	}
}


#endif
