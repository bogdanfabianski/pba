/**
  ******************************************************************************
  * @file           : pbuf_serial.c
  * @brief          : pbuf structures and routines for use of data buffers and transfer operations\
											created because lack of possibility of different sourcing of the data to send in one stream by SPIx 
  ******************************************************************************
	  @author				PP SCADVANCE TEAM C@2019 (B.Fabianski)
									created: 17.06.2019
									v1.0
		@copyright: 	Poznan University of Technology (@2019)
  ******************************************************************************
**/

#include "stm32h7xx_hal.h"
#include "main.h"
#include "ip_parameters.h"
#include "parameters/parameter-type.h"
#include "interconn/pbuf_serial.h"
#include "interconn/hsem.h"
#include "led.h"

extern __IO parameters_t parameters;
extern protocol_selection_t actual_selected_protocol;

extern __IO uint8_t spi1_sending_lock;
extern __IO uint64_t unix_time_us;

extern SPI_HandleTypeDef hspi4;


/************************************* PBUF EHTENRET SECTION *****************************/
	
	
__IO uint8_t free_register_to_set = 4;

pbuf_serial_t pbuf_ethernet_table[ETH_RX_DESC_CNT];

uint8_t Rx_EthCpyBuff[ETH_RX_DESC_CNT][ETH_MAX_PACKET_SIZE+64] __attribute__((aligned (32)));  


pbuf_serial_t* pbuf_ethernet_list_head = NULL;

uint8_t pbuf_ethernet_data_table_init(){
	uint8_t i;

	RTT_LOG("pbuf_ethernet: data table init");	
pbuf_ethernet_list_head = NULL;
	
for(i=0; i<ETH_RX_DESC_CNT; i++){
	pbuf_ethernet_table[i].data_len = 0;
	pbuf_ethernet_table[i].data_pointer = Rx_EthCpyBuff[i];
	pbuf_ethernet_table[i].status = pbuf_serial_status_free;
	pbuf_ethernet_table[i].next = NULL;
	//RTT_LOG("pbuf_serial: vector %x", (uint32_t)pbuf_ethernet_table[i].data_pointer);
}
	free_register_to_set = ETH_RX_DESC_CNT;
return(0);
}

//stage initialisation of DMA transfer (data buffer pointers assignment)
pbuf_serial_t* pbuf_ethernet_data_table_lock_for_dma(){
	uint8_t i;

	//RTT_LOG("pbuf_serial: lock register for DMA/EHR, buffers free: %d",free_register_to_set);	
	for(i=0; i<ETH_RX_DESC_CNT; i++){
		if(pbuf_ethernet_table[i].status == pbuf_serial_status_free){
			pbuf_ethernet_table[i].status = pbuf_serial_status_locked_dma;
			free_register_to_set--;
			//RTT_LOG("pbuf_ethernet: Register taken at address: %x", (uint32_t)pbuf_ethernet_table[i].data_pointer);	
			return(&pbuf_ethernet_table[i]);
		}
	}
//	RTT_LOG("Fail -- no free buffer");	
	return(NULL);
}


//on DMA transfer finished (searchng by address saved in DMA)
pbuf_serial_t* pbuf_ethernet_data_dma_ready(uint8_t* M0AR_val, uint16_t data_len){
	uint8_t i;
	pbuf_serial_t* temp;
	spi_ethernet_data_header* header;
	//RTT_LOG("pbuf_serial: data arrived from DMA (or error handling routine) addr:%x,len:%d",(uint32_t)M0AR_val,data_len);	
	for(i=0; i<ETH_RX_DESC_CNT; i++){
		if(pbuf_ethernet_table[i].data_pointer == M0AR_val){
			//RTT_LOG("data_pointer:%x",(uint32_t)pbuf_serial_table[i].data_pointer);	
			error_handling_hbb_reset();
			pbuf_ethernet_table[i].data_len = data_len;
			header = (spi_ethernet_data_header*)(((uint8_t*)pbuf_ethernet_table[i].data_pointer + data_len));
			//header = pbuf_serial_table[i].data_pointer + data_len));
			//__disable_irq();
			uint64_t val = unix_time_us;
			val+=TIM17->CNT;
		 // __enable_irq();
			//*((uint32_t*)(pbuf_serial_table[i].data_pointer+data_len+8)) = *(uint32_t*)&val;
			//*((uint32_t*)(pbuf_serial_table[i].data_pointer+data_len+12)) = *(uint32_t*)&val;
			
			header->timestamp_l = *((uint32_t*)&val);
	    header->timestamp_h = *(((uint32_t*)&val)+1);
			
			
			//memcpy((uint8_t*)pbuf_serial_table[i].data_pointer + data_len + 8, (uint8_t*)&val,8);
			//header->timestamp = (unix_time_us + TIM17->CNT);
			//RTT_LOG("pbuf_serial: data arrived from DMA: identified");
			//operation on list of data ready to send...
			temp = pbuf_ethernet_list_head;
			if(temp == NULL) {
				pbuf_ethernet_list_head = &pbuf_ethernet_table[i];
				pbuf_ethernet_list_head->next = NULL;
			} else {
				while(temp->next!=NULL){
					temp=temp->next;
				}
				temp->next = &pbuf_ethernet_table[i];
			}
			//end of list opearations
			pbuf_ethernet_table[i].status = pbuf_serial_status_locked_ready_to_send;
			return(&pbuf_ethernet_table[i]);
		}
	}
	RTT_LOG("pbuf_serial: data arrived from DMA: not identified -- ERROR");
	return(NULL);
}

//on DMA transfer error(searchng by address saved in DMA)
pbuf_serial_t* pbuf_ethernet_data_dma_error(uint8_t* M0AR_val){
	uint8_t i;
//	pbuf_serial_t* temp;
	//RTT_LOG("pbuf_serial: data arrived from DMA (or error handling routine)");	
	for(i=0; i<ETH_RX_DESC_CNT; i++){
		if(pbuf_ethernet_table[i].data_pointer == M0AR_val){
			//error_handling_hbb_reset();
			pbuf_ethernet_table[i].data_len = 0x00;
			pbuf_ethernet_table[i].next = NULL;
			pbuf_ethernet_table[i].status = pbuf_serial_status_locked_dma;
			return(&pbuf_ethernet_table[i]);
		}
	}
	RTT_LOG("pbuf_serial: data arrived from DMA: not identified -- ERROR");
	return(NULL);
}

uint32_t CRC_CalculateNew(uint8_t* pBuffer, uint32_t BufferLength);

HAL_StatusTypeDef HAL_SPI_Transmit_DMASPI4(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size);


//prepare data to send...(designed to use in main.h)
pbuf_serial_t* pbuf_ethernet_data_list_get_first_to_send(){

	pbuf_serial_t* temp;
	spi_ethernet_data_header* data_header;
	
	uint16_t ether_type = 0x00;
	    uint8_t ethernet_pointer_offset = 14; 

	//check if thete is DMA free to send the data:
	//check if there is data to send:
	
		temp = pbuf_ethernet_list_head; //remember pointer to data to send via SPI

	//if(HAL_HSEM_Take(HSEM_ID, HSEM_PROCESS_1) == HAL_OK){
		//spi1_sending_lock++;
		__disable_irq();
		if(spi1_sending_lock==0x00){	
		if(pbuf_ethernet_list_head!=NULL && pbuf_ethernet_list_head->next!=NULL){//hspi1.State == HAL_SPI_STATE_READY){
				//RTT_LOG("pbuf_serial: sending data entrance");
			

				temp->status = pbuf_serial_status_locked_sending_via_spi;
				data_header = (spi_ethernet_data_header*)((uint8_t*)(temp->data_pointer)+temp->data_len);
			 
			
				uint8_t* last_buffer_pointer = (uint8_t*)(temp->data_pointer);
			//uint16_t ether_type = 0x00;
			
			data_header->dst_port = 0x00;
			data_header->src_port = 0x00;
			
			if(temp->data_len>=42) 
			{		
			//DESTINATION | SOURCE
			memcpy((uint8_t*)(&(data_header->dst_add)),last_buffer_pointer,12); 
		  
			//802.1Q and ETHER_TYPE
			ether_type = (*(last_buffer_pointer+12)<<8) + *(last_buffer_pointer+13);
		  if(ether_type == 0x8100){
				 data_header->eth_type = (*(last_buffer_pointer+16)<<8) + *(last_buffer_pointer+17);
				ethernet_pointer_offset = 18;
			} else {
				 data_header->eth_type = (*(last_buffer_pointer+12)<<8) + *(last_buffer_pointer+13);
				ethernet_pointer_offset = 14;
			}
			ether_type = data_header->eth_type;

			if(ether_type == 0x0800) { //IPv4
				
				//6	Transmission Control Protocol	TCP
        //17	User Datagram Protocol	UDP
				
				if(*(last_buffer_pointer+ethernet_pointer_offset+9)==6 || *(last_buffer_pointer+ethernet_pointer_offset+9)==17){
					//IHL
					ether_type=(*(last_buffer_pointer+ethernet_pointer_offset+0)&0x0F); 
					ether_type*=4;
					if(ether_type<20){ether_type=20;}
					ethernet_pointer_offset+=ether_type; //skip IP header
					data_header->src_port = (*(last_buffer_pointer+ethernet_pointer_offset+0)<<8) + *(last_buffer_pointer+ethernet_pointer_offset+1);
				  data_header->dst_port = (*(last_buffer_pointer+ethernet_pointer_offset+2)<<8) + *(last_buffer_pointer+ethernet_pointer_offset+3);
				}
				
			} 
				//ERR
				data_header->hard_error = 0x00; //HERR
				data_header->soft_error = 0x00; //ARR
			
		} else {
			data_header->soft_error = 0x01;
		}
			
			
			
			
				//set data len:
				data_header->data_len = temp->next->data_len;
			  //*((uint16_t*)((temp->data_pointer)+(temp->data_len)+UART_RX_LEN_OFFSET)) = temp->next->data_len;
				//
			 data_header->crc = CRC_CalculateNew((uint8_t*)(temp->data_pointer),temp->data_len+ETHERENT_RX_CRC_OFFSET);
		   //data_header->crc = (uint16_t)HAL_CRC_Calculate(&crcHandle, (uint32_t*)temp->data_pointer, temp->data_len+ETHERENT_RX_CRC_OFFSET);
		
				//data_header->crc = HAL_CRC_Calculate(&crcHandle, (uint32_t *)temp->data_pointer, temp->data_len+ETHERENT_RX_CRC_OFFSET);///sizeof(spi_serial_data_header)-2);
			  //*((uint16_t*)((temp->data_pointer)+(temp->data_len)+UART_RX_CRC_OFFSET)) = HAL_CRC_Calculate(&crcHandle, (uint32_t *)temp->data_pointer, temp->data_len+UART_RX_CRC_OFFSET);
			
				//spi1_sending_lock = 1;
			//	RTT_LOG("pbuf_serial: transmitting data... (lock:1)");
				// RTT_LOG("sending data ethernet crc(%d): %x, len:%d, next:%d, address:%x->%x", spi1_sending_lock, data_header->crc,temp->data_len, data_header->data_len,  /*(uint16_t)*(temp->data_pointer+temp->data_len+ETHERENT_RX_LEN_OFFSET),*/
			  // (uint32_t)temp->data_pointer, (uint32_t)temp->next->data_pointer);
				
		   // SCB_CleanInvalidateDCache_by_Addr((uint32_t*)temp->data_pointer, temp->data_len+ETHERENT_RX_OVERHEAD_SIZE+32); //22+32 
	     
				if(HAL_SPI_Transmit_DMASPI4(&hspi4, (uint8_t*)temp->data_pointer, temp->data_len+ETHERENT_RX_OVERHEAD_SIZE) == HAL_OK){
						spi1_sending_lock = 1;
				  __enable_irq();
				
					 #ifndef LOWERING_LED_TX_BLINKING	
	DSIG_RGB_B_GREEN_TOGGLE;
#else
	spi_tx_led_lock=1;
#endif
					
					
					return(temp);
				}
				
			}
				
		} 
  __enable_irq();	
	return(NULL);
}

//after the data are sent via SPI clear pointer...
pbuf_serial_t* pbuf_ethernet_data_list_free_item(){
//  uint8_t i;
	pbuf_serial_t* temp = pbuf_ethernet_list_head;
			//pbuf_serial_list_head=temp->next;;
	//for(i=0; i<FIELD_DATA_RX_BUFF_NUM; i++){
		if(temp->status == pbuf_serial_status_locked_sending_via_spi){
			pbuf_ethernet_list_head = temp->next;
			temp->data_len = 0x00;
		  temp->next = NULL;
			temp->status = pbuf_serial_status_free;
			//RTT_LOG("pbuf_serial: register %x freed", (uint32_t)temp->data_pointer);
			free_register_to_set++;
			return(temp);
		}
	//}
	//RTT_LOG("pbuf_serial: ...ERROR -- no buffer to free!!!");
	return(NULL);
}


//on DMA transfer finished (searchng by address saved in DMA)
pbuf_serial_t* pbuf_ethernet_data_ready(pbuf_serial_t* pbuf_item, uint16_t data_len){

			pbuf_serial_t* temp;
			spi_ethernet_data_header* data_header;
	    
	    //RTT_LOG("pbuf_serial: data arrived from IT (or error handling routine) addr:%x,len:%d",(uint32_t)pbuf_item->data_pointer, data_len);	
			error_handling_hbb_reset();
			pbuf_item->data_len = data_len;
		  uint64_t val = 0x00; 
		  //uint16_t actual_len = pbuf_item->data_len;
			
			//RTT_LOG("last_len: %d", last_len);
			//RTT_LOG("actual_len: %d", actual_len);
			
			if(pbuf_item->data_len>0x600) {
				pbuf_item->data_len=0x600;
			}

			data_header = (spi_ethernet_data_header*)((uint8_t*)(pbuf_item->data_pointer) + data_len);
			//header = pbuf_serial_table[i].data_pointer + data_len));
			//__disable_irq();
			val = unix_time_us;
			val+= TIM17->CNT;
	    //__enable_irq();
			data_header->timestamp_l = *((uint32_t*)&val);
	    data_header->timestamp_h = *(((uint32_t*)&val)+1);
			//memcpy((uint8_t*)pbuf_item->data_pointer + data_len + 12, (uint8_t*)&val,8);
				
		
			
			//operation on list of data ready to send...
			temp = pbuf_ethernet_list_head;
			if(temp == NULL) {
				pbuf_ethernet_list_head = pbuf_item;
				pbuf_ethernet_list_head->next = NULL;
			} else {
				while(temp->next!=NULL){
					temp=temp->next;
				}
				temp->next = pbuf_item;
			}
			//end of list opearations
			pbuf_item->status = pbuf_serial_status_locked_ready_to_send;
			return(pbuf_item);
}


pbuf_serial_t* pbuf_ethernet_data_ready_IRQ(pbuf_serial_t* pbuf_item, uint16_t data_len){

			pbuf_serial_t* temp;
//			spi_ethernet_data_header* header;
	    //RTT_LOG("pbuf_serial: data arrived from IT (or error handling routine) addr:%x,len:%d",(uint32_t)pbuf_item->data_pointer, data_len);	
			error_handling_hbb_reset();
			pbuf_item->data_len = data_len;
//		  uint64_t val = 0x00; 
		  //uint16_t actual_len = pbuf_item->data_len;
			
			//RTT_LOG("last_len: %d", last_len);
			//RTT_LOG("actual_len: %d", actual_len);
			
			if(pbuf_item->data_len>0x600) {
				pbuf_item->data_len=0x600;
			}

			//header = (spi_ethernet_data_header*)(((uint8_t*)pbuf_item->data_pointer + data_len));
			//header = pbuf_serial_table[i].data_pointer + data_len));
			
			/*
			val = unix_time_us;
			val+= TIM17->CNT;
			header->timestamp_l = *((uint32_t*)&val);
	    header->timestamp_h = *(((uint32_t*)&val)+1);
			*/
			//memcpy((uint8_t*)pbuf_item->data_pointer + data_len + 12, (uint8_t*)&val,8);
			//operation on list of data ready to send...
			temp = pbuf_ethernet_list_head;
			if(temp == NULL) {
				pbuf_ethernet_list_head = pbuf_item;
				pbuf_ethernet_list_head->next = NULL;
			} else {
				while(temp->next!=NULL){
					temp=temp->next;
				}
				temp->next = pbuf_item;
			}
			//end of list opearations
			pbuf_item->status = pbuf_serial_status_locked_ready_to_send;
			return(pbuf_item);
}
