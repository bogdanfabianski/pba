/**
  ******************************************************************************
  * @file           : error_handling_comm.c
  * @brief          : Source file "c" with functions of eg. spi error handling
  ******************************************************************************
											PP SCADVANCE TEAM C@2018 (B.Fabianski)
											created: 31.11.2018
											v1.0
  ******************************************************************************
**/

#include "error_handling_comm.h"
#include "crc_routines.h"
#include "commands.h"

#include "netif/ethernet.h"
#include "lwip/def.h"
#include "parameters/parameter-type.h"
#include "parameters/parameter-enum.h"

#include "interconn/pbuf_serial.h"
#include "interconn/hsem.h"

static __IO uint8_t error_handling_hbb_time_ticks = 0;

extern parameters_t parameters;

extern CRC_HandleTypeDef crcHandle;
 

extern SPI_HandleTypeDef hspi4;

//__IO uint8_t error_cnt = 0;
		
#ifdef EHERNET_INPUT_MODIFICATION
	extern struct pbuf * pbuf_actual;
	extern struct pbuf * pbuf_last;
	extern uint8_t zero_frame[50];
#endif


extern __IO uint8_t spi1_sending_lock;

uint32_t CRC_CalculateNew(uint8_t* pBuffer, uint32_t BufferLength);

void HAL_ETH_MACErrorCallback(ETH_HandleTypeDef *heth){
	
	RTT_LOG("PBA: HAL_ETH_MACErrorCallback error no:%d",heth->MACErrorCode);	
}

void HAL_ETH_DMAErrorCallback(ETH_HandleTypeDef *heth)
{
	//RTT_LOG("ETH ERROR DMA:%d, ETH:%d", heth->DMAErrorCode, heth->ErrorCode);
	
//  pbuf_serial_t* temp;
  ETH_BufferTypeDef RxBuff;
//  uint32_t framelength = 0;
//	struct pbuf_custom* custom_pbuf;
	HAL_ETH_GetRxDataBuffer(heth, &RxBuff);
	HAL_ETH_BuildRxDescriptors(heth);
}


uint8_t error_handling_hbb_tick(){
	error_handling_hbb_time_ticks++;
	if(error_handling_hbb_time_ticks>ERROR_HANDLING_HBB_PERIOD){
			error_handling_hbb_reset();
 //uint16_t last_len;
			//uint8_t* last_buffer_pointer;
//			uint32_t alignedAddr;
	if(parameters.list_s.peripheral_s.interface.value == peripheral_interface_ethernet_e && \
		parameters.list_s.peripheral_s.ethernet_s.operation_mode.value == ethernet_operation_mode_passive){	 
	    pbuf_serial_t* temp = pbuf_ethernet_data_table_lock_for_dma();
			if(temp!=NULL){
					pbuf_ethernet_data_ready_IRQ(temp, 0x00);
					//RTT_LOG("HBB reset, reg: %X, len:%d",(uint32_t)(temp->data_pointer),temp->data_len);
				RTT_LOG("HBA-ET");
				//pbuf_ethernet_data_list_get_first_to_send();}
				return(1); 
			}
	}
}

	return(0);
}

void error_handling_hbb_reset(){
	error_handling_hbb_time_ticks = 0;
}

extern SPI_HandleTypeDef hspi5;
extern char* cmd_string_ascii;
extern __IO uint8_t spi1_sending_lock;

void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi)
{
	if(hspi->Instance == SPI4){
			RTT_LOG("spi error callback SPI4 code: %d", hspi->ErrorCode);	
			#ifdef EHERNET_INPUT_MODIFICATION
		spi1_sending_lock = 0;
	    #endif
	} else if(hspi->Instance == SPI5){
		RTT_LOG("spi error callback SPI5, code:%d", hspi->ErrorCode);
	  HAL_SPI_Receive_IT(&hspi5,(uint8_t*)cmd_string_ascii, CMD_DATA_MAX_SIZE);
	}
}

//  HAL_UART_ERROR_NONE      = 0x00U,    /*!< No error            */
//  HAL_UART_ERROR_PE        = 0x01U,    /*!< Parity error        */
//  HAL_UART_ERROR_NE        = 0x02U,    /*!< Noise error         */
//  HAL_UART_ERROR_FE        = 0x04U,    /*!< frame error         */
//  HAL_UART_ERROR_ORE       = 0x08U,    /*!< Overrun error       */
//  HAL_UART_ERROR_DMA       = 0x10U     /*!< DMA transfer error  */


