/**
  ******************************************************************************
  * @file           : inter_prot_parameters.c
  * @brief          : Source file "c" with functions of interface/protocol (ip) parameters set
  ******************************************************************************
					PP SCADVANCE TEAM C@2019 (B.Fabianski)
					created: 02.03.2019
					v1.0
  ******************************************************************************
**/

#include "parameters/parameter-setup-const.h"
#include "parameters/parameter-enum.h"
#include "ip_parameters.h"
#include "commands.h"
#include "parameters/parameter-type.h"

extern parameters_t parameters;

extern protocol_selection_t actual_selected_protocol;

ip_parameters_t ip_parameters;

ip_parameters_t* ip_get_param_structure(){
	return(&ip_parameters);
}

uint8_t* ip_get_param_structure_serialized(){
	return((uint8_t*)(&ip_parameters));
}

uint32_t ip_get_param_structure_size(){
	return(sizeof(ip_parameters_t));
}

void ip_set_param_from_serial_data(uint8_t* serial_data){
	ip_parameters =  *((ip_parameters_t*)serial_data);
}

void ip_init_params(){
	ip_parameters.interface = (interface_selection_t)parameters.list_s.peripheral_s.interface.value;
	ip_parameters.protocol =  (protocol_selection_t)parameters.list_s.peripheral_s.protocol.value;
	actual_selected_protocol = (protocol_selection_t)parameters.list_s.peripheral_s.protocol.value;
}


protocol_selection_t ip_selected_protocol(){
	return(ip_parameters.protocol);
}	

interface_selection_t ip_selected_interface(){
	return(ip_parameters.interface);
}	

void ip_set_params(interface_selection_t interface,	 protocol_selection_t protocol)
{
	   ip_parameters.interface  = interface;
	   ip_parameters.protocol   = protocol;
//		 command_send((commands_type)commands_type_interface_protocol, ip_get_param_structure_serialized(), ip_get_param_structure_size()); 
}
