/**
  ******************************************************************************
  * @file           : ethernet_parameters.c
  * @brief          : Source file "c" with functions of ethernet PA/PB parameters set
	* @author 				: Bogdan Fabianski
  ******************************************************************************
					PP SCADVANCE TEAM C@2019 (B.Fabianski)
					created: 10.06.2019
					v1.0
  ******************************************************************************
**/

#include "ethernet_parameters.h"
#include "commands.h"
#include "error_handling_comm.h"

#include "parameters/parameter.h"


ethernet_parameters_t ethernet_parameters;

ethernet_parameters_t* ethernet_get_param_structure(){
	return(&ethernet_parameters);
}

uint8_t* ethernet_get_param_structure_serialized(){
	return((uint8_t*)(&ethernet_parameters));
}

uint32_t ethernet_get_param_structure_size(){
	return(sizeof(ethernet_parameters_t));
}

void ethernet_set_param_from_serial_data(uint8_t* serial_data){
	ethernet_parameters =  *((ethernet_parameters_t*)serial_data);
}

