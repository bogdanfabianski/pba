
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
	* @author					: Bogdan Fabianski
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 Poznan University of Technology .
  * All rights reserved.</center></h2>
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "led.h"
#include "parameters/parameter-nvm.h"

extern __IO uint8_t parameters_save_flag;
extern struct netif gnetif;
extern IWDG_HandleTypeDef hiwdg1;

__IO uint8_t old_led_delay = 0;

void ledProcess(__IO uint8_t* led_delay){

	  if(old_led_delay!=*led_delay){
			
		#ifdef USE_IWDG
			HAL_IWDG_Refresh(&hiwdg1);
//			RTT_LOG("IWDG_REF");
		#endif
			old_led_delay=*led_delay;
		switch(*led_delay){
			case 20: DSIG_RGB_B_BLUE(1); *led_delay=0; break;
			case 1:  DSIG_RGB_B_BLUE(0); break;
		}
#ifdef LOWERING_LED_TX_BLINKING	
		if(spi_tx_led_lock){
			//if(spi_tx_led_lock==1){
			//	DSIG_RGB_B_GREEN(1); spi_tx_led_lock = 2;
			//} else {	DSIG_RGB_B_GREEN(0); spi_tx_led_lock=0;}
			spi_tx_led_lock=0;
			DSIG_RGB_B_GREEN_TOGGLE;
			//DSIG_RGB_B_RED_TOGGLE;
		}
#endif
	
		//ethernet_link_check_state(&gnetif);
	
if(parameters_save_flag){
			parameter_nvm_save();
			parameters_save_flag=0x00;
}

}
}

/************************ (C) COPYRIGHT Poznan University of Technology *****END OF FILE****/
