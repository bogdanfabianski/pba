/**
  ******************************************************************************
  * @file           : ingest_ethernet_input.c
  * @brief          : ethernet input processing functions set (and relatives) for ETH frame processing
	
  ******************************************************************************
					PP SCADVANCE TEAM C@2019 (B.Fabianski)
					created: 13.05.2019
					v1.0 
					
  ******************************************************************************
**/

#include "main.h"
#include "ingest_ethernet_input.h"
#include "crc_routines.h"
#include "lwip/opt.h"

#include "netif/ethernet.h"
#include "lwip/def.h"
#include "lwip/stats.h"
#include "lwip/etharp.h"
#include "lwip/ip.h"
#include "lwip/snmp.h"

#include "led.h"

#include "error_handling_comm.h"

#include <string.h>

#include "netif/ppp/ppp_opts.h"
#if PPPOE_SUPPORT
#include "netif/ppp/pppoe.h"
#endif /* PPPOE_SUPPORT */

#ifdef LWIP_HOOK_FILENAME
#include LWIP_HOOK_FILENAME
#endif

#ifdef EHERNET_INPUT_MODIFICATION

extern CRC_HandleTypeDef hcrc;
extern SPI_HandleTypeDef hspi4;

extern __IO uint8_t spi_tx_led_lock;

uint8_t zero_frame[64] __attribute__((aligned (32)));

struct pbuf * pbuf_actual = NULL;
struct pbuf * pbuf_last   = NULL;


extern char all_ascii[];

__IO uint64_t unix_time_us;



void  etherntet_input_reset(void){
	uint8_t index = 0;
pbuf_actual = NULL;
pbuf_last   = NULL;	
	for(index=0; index<30; index++){
			*(zero_frame+index)=0x00;
	}
}


uint32_t CRC_CalculateNew(uint8_t* pBuffer, uint32_t BufferLength)
{
  uint32_t BufferLen; /* CRC input data buffer index  -- limited to lat 32 bytes */
//  uint32_t temp = 0;  /* CRC output (read from hcrc->Instance->DR register) */
	uint32_t i = 0; /* input data buffer index */
	uint8_t* pBufferLoc;
	
	
	if(BufferLength<=32){
		BufferLen = BufferLength;
		pBufferLoc = pBuffer;
	} else {
		BufferLen = 32;
		pBufferLoc = pBuffer+(BufferLength-BufferLen);
	}
	 

    
  /* Process locked */
  __HAL_LOCK(&hcrc); 
  
  /* Change CRC peripheral state */  
  (&hcrc)->State = HAL_CRC_STATE_BUSY;
  
  /* Reset CRC Calculation Unit (hcrc->Instance->INIT is 
  *  written in hcrc->Instance->DR) */
  __HAL_CRC_DR_RESET(&hcrc);
  
	
	/*
	for(i = 0; i < (BufferLen>>2); i++) //TODO: if it can be easily switched to 32-bit operations?????
   {
     //(&hcrc)->Instance->DR = (uint32_t)(((uint32_t)(pBuffer[4*i])<<24) | ((uint32_t)(pBuffer[4*i+1])<<16) | ((uint32_t)(pBuffer[4*i+2])<<8) | (uint32_t)(pBuffer[4*i+3]));
		 (hcrc)->Instance->DR = *(pBufferLoc + i);
   }
  
	 i=(i<<2);
	 
	 pBufferLocCh = ((uint8_t*)pBufferLoc + i);
   if((BufferLen%4) != 0)
   {
     if(BufferLen%4 == 1)
     {
       *(__IO uint8_t*) ((hcrc)->Instance->DR) = *pBufferLocCh;
     } else 
     if(BufferLen%4 == 2)
     {
       *(__IO uint16_t*) ((hcrc)->Instance->DR) = ((((uint16_t)*(pBufferLocCh))<<8) | (((uint16_t)*(pBufferLocCh+1))<<8));       
     } else 
     if(BufferLen%4 == 3)
     {
       *(__IO uint16_t*) ((hcrc)->Instance->DR) = ((((uint16_t)*(pBufferLocCh))<<8) | (((uint16_t)*(pBufferLocCh+1))<<8)); 
       *(__IO uint8_t*) ((hcrc)->Instance->DR) =  *(pBufferLocCh+2);    
     }
   }
  */

  
   /* Processing time optimization: 4 bytes are entered in a row with a single word write,
    * last bytes must be carefully fed to the CRC calculator to ensure a correct type
    * handling by the IP */
   for(i = 0; i < (BufferLen/4); i++)
   {
     (&hcrc)->Instance->DR = (uint32_t)(((uint32_t)(pBufferLoc[4*i])<<24) | ((uint32_t)(pBufferLoc[4*i+1])<<16) | ((uint32_t)(pBufferLoc[4*i+2])<<8) | (uint32_t)(pBufferLoc[4*i+3]));
   }
   /* last bytes specific handling */
   if((BufferLen%4) != 0)
   {
     if(BufferLen%4 == 1)
     {
       *(__IO uint8_t*) ((&hcrc)->Instance->DR) = pBufferLoc[4*i];
     }
     if(BufferLen%4 == 2)
     {
       *(__IO uint16_t*) ((&hcrc)->Instance->DR) = (uint16_t)((uint16_t)((uint16_t)(pBufferLoc[4*i])<<8) | (uint16_t)(pBufferLoc[4*i+1]));       
     }
     if(BufferLen%4 == 3)
     {
       *(__IO uint16_t*) ((&hcrc)->Instance->DR) = (uint16_t)((uint16_t)((uint16_t)(pBufferLoc[4*i])<<8) | (uint16_t)(pBufferLoc[4*i+1]));
       *(__IO uint8_t*) ((&hcrc)->Instance->DR) = pBufferLoc[4*i+2];       
     }
   }
  
  /* Return the CRC computed value */ 
  BufferLen = ((&hcrc)->Instance->DR);
	
  /* Change CRC peripheral state */
  (&hcrc)->State = HAL_CRC_STATE_READY;

  /* Process unlocked */
  __HAL_UNLOCK(&hcrc);
  

  /* Return the CRC computed value */ 
  return(BufferLen);
}


void ethernet_input_clear_last_buff(void){
	
		if(pbuf_last!=NULL){
		    ((struct pbuf_custom*)pbuf_last)->custom_free_function(pbuf_last); // to check if this not releases rx buffer BEFORE the data is sent via SPI
		pbuf_last = NULL; 
    }
	  pbuf_last = pbuf_actual;
}

__IO uint8_t spi1_sending_lock = 0x00;
__IO uint8_t incomming_transfer_count = 0x00;

__IO uint64_t unix_time_us;

err_t etherntet_input_process(struct pbuf *p, struct netif *netif){

	    uint16_t last_len;
	    uint8_t i=0;
			uint8_t* last_buffer_pointer;
//      uint32_t alignedAddr;
	    uint16_t ether_type = 0x00;
	    uint8_t ethernet_pointer_offset = 14; 
	    //char log_temp[30];
	
	    error_handling_hbb_reset();	
	
	    while(spi1_sending_lock>0){HAL_Delay(1); i++;} //{if(spi1_sending_lock<2){spi1_sending_lock++; RTT_LOG("czekam");}  }
	    //SEGGER_RTT_WriteString(0, "Data in\n");
		  pbuf_actual = p;
	   
	    RTT_LOG("NEW DATA IN\n pbuf_last:%d, pbuf_act:%d, number:%d", (uint32_t)pbuf_last->payload, (uint32_t)pbuf_actual->payload, incomming_transfer_count++);
	    
	    if(pbuf_last==pbuf_actual || pbuf_last==NULL){
				last_len = 0;
				last_buffer_pointer = zero_frame;
				
    	} else {
				last_len = pbuf_last->len;
				last_buffer_pointer = pbuf_last->payload;
			}

			uint16_t actual_len = pbuf_actual->len;
			
			RTT_LOG("last_len: %d", last_len);
			RTT_LOG("actual_len: %d", actual_len);
			
			
		  *(last_buffer_pointer+last_len+18) = (&hspi4)->ErrorCode; //HERR
			*(last_buffer_pointer+last_len+19) = 0x00; //AERR
			//#warning: IN RELEASE V. CHANGE THE DESTIANTION OF CALL:		
			if(actual_len>0x600) {
			 //strcpy(log_temp, "BC,L:0x0000\n");
			 //memcpy((log_temp+7), (char*) (all_ascii+2*((uint8_t)(actual_len>>8))),2);
			 //memcpy((log_temp+9), (char*) (all_ascii+2*((uint8_t)(actual_len))),2);
			 RTT_LOG("size>0x600"); 
			 //memcpy((log_temp+17), (char*) (all_ascii+2*((uint8_t)(received_data_len_next>>8))),2);
			 //memcpy((log_temp+19), (char*) (all_ascii+2*((uint8_t)(received_data_len_next))),2);
				
				actual_len=0x600; pbuf_actual->len=0x600;
			}
		  //uint8_t* actual_buffer = pbuf_actual->payload; //pointer to actual len of the stack for incomming data
	
	
	    //0-5 MAC_SRC, 6-11 - MAC_DST, 12-13 ETHER_TYPE, 14-15 PORT_SRC, 16-17 PORT_DST, 18-19 ERR, 20-21 DLEN, 22-23 CRC 
	    //Ethernet tail set to 24 bytes 
	    //*(last_buffer_pointer+last_len+20) = 0x00;
			//change to 16 bytes format
			//PORTS nr reset:
			
			spi_ethernet_data_header* ethernet_header = (spi_ethernet_data_header*)(last_buffer_pointer+last_len);
			
			uint64_t val = unix_time_us;
			val+= TIM17->CNT;
			//*((uint32_t*)(pbuf_serial_table[i].data_pointer+data_len+8)) = *(uint32_t*)&val;
			//*((uint32_t*)(pbuf_serial_table[i].data_pointer+data_len+12)) = *(uint32_t*)&val;
			
			ethernet_header->timestamp_l = *((uint32_t*)&val);
	    ethernet_header->timestamp_h = *(((uint32_t*)&val)+1);
			
		  ethernet_header->dst_port = 0x00;
			ethernet_header->src_port = 0x00;
			
			ethernet_header->data_len = actual_len; //critical phase - last data are transferred, at new length pointer	
	    
			if(last_len>=42) 
			{		
			//DESTINATION | SOURCE
			memcpy((uint8_t*)(&(ethernet_header->dst_add)),last_buffer_pointer,12); 
		  
			//802.1Q and ETHER_TYPE
			ether_type = (*(last_buffer_pointer+12)<<8) + *(last_buffer_pointer+13);
		  if(ether_type == 0x8100){
				 ethernet_header->eth_type = (*(last_buffer_pointer+16)<<8) + *(last_buffer_pointer+17);
				ethernet_pointer_offset = 18;
			} else {
				 ethernet_header->eth_type = (*(last_buffer_pointer+12)<<8) + *(last_buffer_pointer+13);
				ethernet_pointer_offset = 14;
			}
			ether_type = ethernet_header->eth_type;

			if(ether_type == 0x0800) { //IPv4
				
				//6	Transmission Control Protocol	TCP
        //17	User Datagram Protocol	UDP
				
				if(*(last_buffer_pointer+ethernet_pointer_offset+9)==6 || *(last_buffer_pointer+ethernet_pointer_offset+9)==17){
					//IHL
					ether_type=(*(last_buffer_pointer+ethernet_pointer_offset+0)&0x0F); 
					ether_type*=4;
					if(ether_type<20){ether_type=20;}
					ethernet_pointer_offset+=ether_type; //skip IP header
					ethernet_header->src_port = (*(last_buffer_pointer+ethernet_pointer_offset+0)<<8) + *(last_buffer_pointer+ethernet_pointer_offset+1);
				  ethernet_header->dst_port = (*(last_buffer_pointer+ethernet_pointer_offset+2)<<8) + *(last_buffer_pointer+ethernet_pointer_offset+3);
				}
				
			} 
				//ERR
				ethernet_header->hard_error = 0x00; //HERR
				ethernet_header->soft_error = 0x00; //ARR
			
		} else {
			
			if(last_len>12){
				memcpy((uint8_t*)(&(ethernet_header->dst_add)),last_buffer_pointer,12);
				memset((uint8_t*)(&(ethernet_header->eth_type)),0x00,6);				
			} else {
				memset(last_buffer_pointer+(last_len+0),0x00,18);
			}
			
			ethernet_header->hard_error = 0x00; //HERR
			ethernet_header->soft_error = 0x01; //AERR
		}
		

			//add CRC for SPI
			*((uint16_t*)(last_buffer_pointer+(last_len+ETHERENT_RX_CRC_OFFSET))) = CRC_CalculateNew(last_buffer_pointer, last_len+ETHERENT_RX_CRC_OFFSET);
			
			//alignedAddr = (uint32_t)last_buffer_pointer &  ~0x1F;
			
			SCB_CleanInvalidateDCache_by_Addr((uint32_t*)last_buffer_pointer, last_len+ETHERENT_RX_OVERHEAD_SIZE+32); //22+32 
		
		  //transmitting DMA
	    spi1_sending_lock = 1;
		/*
		  if(last_len > 0x600){
				RTT_LOG("Tx Data too large");
		     //SEGGER_RTT_WriteString(0, "Tx Data too large");
			}
		*/
		  if(last_len > 0x600){
		     SEGGER_RTT_WriteString(0, "Tx Data too large");
			}
			HAL_SPI_Transmit_DMA(&hspi4, (uint8_t*)last_buffer_pointer, last_len+ETHERENT_RX_OVERHEAD_SIZE);
	
//data analitics...	
     		  
	
#ifndef LOWERING_LED_TX_BLINKING	
	DSIG_RGB_B_GREEN_TOGGLE;
#else
	spi_tx_led_lock=1;
#endif

		 
	return ERR_OK;

// free_and_return:
//  pbuf_free(p);
//  return ERR_OK; 	
	
}

err_t ethernet_input(struct pbuf *p, struct netif *netif){
	return(etherntet_input_process(p,netif));
}

#endif
