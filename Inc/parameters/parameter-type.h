#ifndef _PARAMETER_TYPE_H_
#define _PARAMETER_TYPE_H_
/**
*	@author Dominik Luczak, Bogdan Fabianski
*	@date 2018-09-25
*	@brief Parameter type
*/
#include "parameters/parameter-type-common.h"
	
//List of parameters stored in NVM
typedef struct{

  uint32_t  struct_version;	///< Parameter version fo structure - changed by programmer
  uint32_t	count;	///< Number of parameters
  uint32_t	size;	///< Size of parameters struct
	uint8_t   refresh; ///<flag of refresh and sign what to refresh
  
  struct{
	  //Peripherial board
	   struct {
		  parameter_select_t interface;
			parameter_select_t protocol;
			struct{
				 parameter_mac_address_t macPA;			
				 parameter_select_t operation_mode;
			}ethernet_s;		
	  }peripheral_s;
		//Dummy parameter only for debug
		parameter_uint32_t dummy;
  }list_s;
  
uint32_t crc32;	///< CRC32 check code of parameters	- must be last in structure
uint8_t last_save_status;
uint8_t load_defaults; //added to check if default values should be restored
	
}parameters_t; 	///< Parameters structure

#endif
