#ifndef _PARAMETER_H_
#define _PARAMETER_H_
/**
*	@author Dominik Luczak, Bogdan Fabianski
*	@date 2018-09-25
*	@brief Parameters
*/
#include "parameter-type.h"

#define MAGIC_NUMBER_LOAD_DEFAULTS 0xAB	

extern parameters_t parameters;

/**
 * Find parameter by id
 * @param[in] id Id of parameter
 * @return Pointer to parameter. Return 0 if not found.
 */
parameter_t* parameter_find_by_id(uint32_t id);

/**
 * Set pointed parameter value
 * @param[in] id Id of parameter
 * @param[in] value_string String containing value to be set
 * @return Pointer to parameter. Return 0 if not found. 
  */
parameter_t* parameter_set_value_by_id(uint32_t id, char* value_string);


/**
 * Get pointer to first parameter
 * @return Pointer to first parameter
 */
parameter_t* parameter_get_fisrt(void);


/**
 * Get pointer to next parameter
 * @param[in] pParameter Pointer to parameter
 * @return Pointer to next parameter. 0 if end.
 */
parameter_t* parameter_get_next_parameter(parameter_t* pParameter);

uint8_t parameter_need_restore(void);
void parameter_set_restore(void);
uint8_t parameter_get_save_status(void);
void parameter_reset_restore(void);
void parameter_set_last_save_status(uint8_t status);

void parameter_setup(void);
 

#endif
