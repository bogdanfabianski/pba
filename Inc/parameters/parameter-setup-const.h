#ifndef _PARAMETER_SETUP_CONST_H_
#define _PARAMETER_SETUP_CONST_H_
/**
*	@author Dominik Luczak, Bogdan Fabianski
*	@date 2019-01-03 / mod. 2019-03
*	@brief Parameter setup constant values
*/
#include <stdint.h>
#include "parameters/parameter-enum.h"

#define _DEVICE_01 

/*INTERFACE/PROTOCOL PARAMETERS*/

#define DEFAULT_INTERFACE peripheral_interface_ethernet_e
#define DEFAULT_PROTOCOL serial_protocol_raw_e
#define PA_PB_ETHERNET_OPERATION_MODE ethernet_operation_mode_passive

#define PB_MAC_ADDR0   0x40
#define PB_MAC_ADDR1   0x6c
#define PB_MAC_ADDR2   0x6d
#define PB_MAC_ADDR3   0x61

#define PA_MAC_ADDR0   PB_MAC_ADDR0
#define PA_MAC_ADDR1   PB_MAC_ADDR1
#define PA_MAC_ADDR2   PB_MAC_ADDR2
#define PA_MAC_ADDR3   PB_MAC_ADDR3
#define PA_MAC_ADDR4   0x31
#define PA_MAC_ADDR5   0xE0

#define PB_IP_ADDR0   192
#define PB_IP_ADDR1   168
#define PB_IP_ADDR2   124

#ifdef _DEVICE_01
#define PB_MAC_ADDR4   0x54
#define PB_MAC_ADDR5   0x15
#define PB_IP_ADDR3   200
#endif

#ifdef _DEVICE_02
#define PB_MAC_ADDR4   0x35
#define PB_MAC_ADDR5   0x0D
#define PB_IP_ADDR3   201
#endif

#ifdef _DEVICE_03
#define PB_MAC_ADDR4   0xA2
#define PB_MAC_ADDR5   0x61
#define PB_IP_ADDR3   202
#endif

#ifdef _DEVICE_04
#define PB_MAC_ADDR4   0x46
#define PB_MAC_ADDR5   0xDF
#define PB_IP_ADDR3   203
#endif

#ifdef _DEVICE_05
#define PB_MAC_ADDR4   0x3C
#define PB_MAC_ADDR5   0xB1
#define PB_IP_ADDR3   204
#endif

/*NETMASK*/
#define PB_NETMASK_ADDR0   255
#define PB_NETMASK_ADDR1   255
#define PB_NETMASK_ADDR2   255
#define PB_NETMASK_ADDR3   0

/*Gateway Address*/
#define PB_GW_ADDR0   192
#define PB_GW_ADDR1   168
#define PB_GW_ADDR2   124
#define PB_GW_ADDR3   1 

#define PA_PB_ETHERNET_PROTOCOL  ethernet_protocol_raw_e

#endif


