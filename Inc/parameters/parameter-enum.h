#ifndef _PARAMETER_ENUM_H_
#define _PARAMETER_ENUM_H_
/**
*	@author Dominik Luczak, Bogdan Fabianski
*	@date 2018-09-25
*	@brief Parameter enums
*/


enum peripheral_interface_t{
	peripheral_interface_disable_e=0,
	peripheral_interface_rs485_e,
	peripheral_interface_rs232_e,
	peripheral_interface_rs485AB_e,
	peripheral_interface_can_e,
	peripheral_interface_ethernet_e
};

// 0x1002 -- baudrate -- not enumerated values, 38400, "uart1.baudrate", "Baudrate in bps."
	
// UART1_STOP,0x1003, "uart1.stop_bit", "{\\\"d\\\":\\\"Stop bit length\\\",\\\"i\\\":[\\\"0.5 bit\\\",\\\"1 bit\\\",\\\"1.5 bit\\\",\\\"2 bit\\\"]}");
enum parameter_uart_stop_bits{ 
	uart_stop_bit_0_5_e = 0,
	uart_stop_bit_1_e,
	uart_stop_bit_1_5_e,
	uart_stop_bit_2_e
};

//UART1_PARITY, 0x1004, "uart1.parity", "{\\\"d\\\":\\\"Parity\\\",\\\"i\\\":[\\\"Disabled\\\",\\\"Odd\\\",\\\"Even\\\"]}");
enum parameter_uart_parity{
	uart_parity_disabled_e = 0,
	uart_parity_odd_e,
	uart_parity_even_e
};

//UART1_LENGTH, 0x1005, "uart1.data_length", "{\\\"d\\\":\\\"Data length\\\",\\\"i\\\":[\\\"7 bit\\\",\\\"8 bit\\\",\\\"9 bit\\\"]}");
enum parameter_uart_data_length{
	uart_data_len_7_e = 0,
	uart_data_len_8_e,
	uart_data_len_9_e
};


//SERIAL_PROTOCOL, 0x1006, "{\\\"d\\\":\\\"Serial protocol\\\",\\\"i\\\":[\\\"Raw\\\",\\\"Modbus\\\",\\\"Profibus\\\"]}"
enum peripheral_protocol{
	serial_protocol_raw_e = 0,
	serial_protocol_modbus_e,
	serial_protocol_profibus_e,
	serial_protocol_DNP3_e,
	serial_protocol_reserved4,
	serial_protocol_reserved5,
	serial_protocol_reserved6,
	serial_protocol_reserved7,
	serial_protocol_reserved8,
	serial_protocol_reserved9,
	ethernet_protocol_raw_e = 10,
	ethernet_protocol_modbus_tcpip_e,
	ethernet_protocol_profinet_e,
	ethernet_protocol_powerlink_e,
	ethernet_protocol_reserved14,
	ethernet_protocol_reserved15
};


enum parameter_ethernet_operation_mode{
	ethernet_operation_mode_active = 0,
	ethernet_operation_mode_passive
};

#endif
