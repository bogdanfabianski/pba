#ifndef _FLASH_H_
#define _FLASH_H_
/**
*	@author Dominik Luczak
*	@date 2018-10-20
*	@brief Flash memory
*/

#include <stdint.h>

// Base address of the Flash sectors
#ifdef STM32H753xx
//Based on https://www.st.com/content/ccc/resource/technical/document/reference_manual/group0/c9/a3/76/fa/55/46/45/fa/DM00314099/files/DM00314099.pdf/jcr:content/translations/en.DM00314099.pdf
//Page 138

	#define FLASH_BASE_ADDR      (uint32_t)(FLASH_BASE)
	#define FLASH_END_ADDR       (uint32_t)(0x081FFFFF)

	/* Base address of the Flash sectors Bank 1 */
	#define ADDR_FLASH_SECTOR_0_BANK1     ((uint32_t)0x08000000) /* Base @ of Sector 0, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_1_BANK1     ((uint32_t)0x08020000) /* Base @ of Sector 1, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_2_BANK1     ((uint32_t)0x08040000) /* Base @ of Sector 2, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_3_BANK1     ((uint32_t)0x08060000) /* Base @ of Sector 3, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_4_BANK1     ((uint32_t)0x08080000) /* Base @ of Sector 4, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_5_BANK1     ((uint32_t)0x080A0000) /* Base @ of Sector 5, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_6_BANK1     ((uint32_t)0x080C0000) /* Base @ of Sector 6, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_7_BANK1     ((uint32_t)0x080E0000) /* Base @ of Sector 7, 128 Kbytes */

	/* Base address of the Flash sectors Bank 2 */
	#define ADDR_FLASH_SECTOR_0_BANK2     ((uint32_t)0x08100000) /* Base @ of Sector 0, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_1_BANK2     ((uint32_t)0x08120000) /* Base @ of Sector 1, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_2_BANK2     ((uint32_t)0x08140000) /* Base @ of Sector 2, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_3_BANK2     ((uint32_t)0x08160000) /* Base @ of Sector 3, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_4_BANK2     ((uint32_t)0x08180000) /* Base @ of Sector 4, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_5_BANK2     ((uint32_t)0x081A0000) /* Base @ of Sector 5, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_6_BANK2     ((uint32_t)0x081C0000) /* Base @ of Sector 6, 128 Kbytes */
	#define ADDR_FLASH_SECTOR_7_BANK2     ((uint32_t)0x081E0000) /* Base @ of Sector 7, 128 Kbytes */	
	
	//#define FLASH_USER_START_ADDRESS   ADDR_FLASH_SECTOR_7   // Start @ of user Flash area
	//#define FLASH_USER_END_ADDRESS     ADDR_FLASH_SECTOR_7   // End @ of user Flash area
	
	#define FLASH_USER_START_ADDR   ADDR_FLASH_SECTOR_7_BANK1      /* Start @ of user Flash area Bank1 */
	#define FLASH_USER_END_ADDR     (ADDR_FLASH_SECTOR_7_BANK1+128*1024-1)  /* End @ of user Flash area Bank1*/
	
#endif


/**
* Write data to internal flash memory
* @param[in]  pData Pointer to data
* @param[in]  size Size of data in bytes
* @return Return 0 when succeed, 1 - fail
*/
uint8_t flash_write(uint32_t *pData, uint32_t size);

#endif
