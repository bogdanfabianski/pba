#ifndef _PARAMETER_NVM_H_
#define _PARAMETER_NVM_H_
/**
*	@author Dominik Luczak
*	@date 2018-10-20
*	@brief Parameters non-volatile memory
*/

#include <stdint.h>

/**
* Save parameters in non-volatile memory
* @return Return 0 when succeed, 1 - fail
* @warning Initialize parameters structure before use of this function. Initialize HAL-CRC first.
*/
uint8_t parameter_nvm_save(void);

/**
* Load parameters from non-volatile memory
* @warning Initialize parameters structure before use of this function. Initialize HAL-CRC first.
*/
void parameter_nvm_load(void);

#endif
