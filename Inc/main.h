/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"
#include <string.h>
#include <stdio.h>
#include "SEGGER_RTT.h"
//#include "parameters/parameter-setup-const.h"
#include "lan8742.h"
#include "headers_structures.h"

#define EHERNET_INPUT_MODIFICATION	
//#define RTT_ENABLED 1

#define CMD_DATA_MAX_SIZE 200

//#define PPM_STANDALONE_TEST 1
//#define CHECK_SYSTEM_LOAD 1
//#define PARTIAL_SYSTEM_LOAD_TEST 1


#ifndef PPM_STANDALONE_TEST
#define USE_IWDG 1
#endif

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define SPI_DAT1_CLK_Pin GPIO_PIN_2
#define SPI_DAT1_CLK_GPIO_Port GPIOE
#define SPI_DAT1_NSS_Pin GPIO_PIN_4
#define SPI_DAT1_NSS_GPIO_Port GPIOE
#define SPI_DAT1_MOSI_Pin GPIO_PIN_6
#define SPI_DAT1_MOSI_GPIO_Port GPIOE
#define SPI5_CMD_NSS_Pin GPIO_PIN_6
#define SPI5_CMD_NSS_GPIO_Port GPIOF
#define SPI5_CMD_CLK_Pin GPIO_PIN_7
#define SPI5_CMD_CLK_GPIO_Port GPIOF
#define SPI5_CMD_MOSI_Pin GPIO_PIN_9
#define SPI5_CMD_MOSI_GPIO_Port GPIOF
#define GPIO_DSIG_OUT_LAN_RST_Pin GPIO_PIN_0
#define GPIO_DSIG_OUT_LAN_RST_GPIO_Port GPIOA
#define GPIO_OUT_RGB_GREEN_Pin GPIO_PIN_8
#define GPIO_OUT_RGB_GREEN_GPIO_Port GPIOD
#define GPIO_OUT_RGB_BLUE_Pin GPIO_PIN_9
#define GPIO_OUT_RGB_BLUE_GPIO_Port GPIOD
#define GPIO_OUT_RGB_RED_Pin GPIO_PIN_10
#define GPIO_OUT_RGB_RED_GPIO_Port GPIOD
#define GPIO_IN_SIG_LOCK_Pin GPIO_PIN_5
#define GPIO_IN_SIG_LOCK_GPIO_Port GPIOB
#define GPIO_OUT_BOOT0_CTRL_Pin GPIO_PIN_7
#define GPIO_OUT_BOOT0_CTRL_GPIO_Port GPIOB

#define GPIO_DSIG_OUT_LAN_nRST(z)							 	(z ? (GPIO_DSIG_OUT_LAN_RST_GPIO_Port->BSRRL = GPIO_DSIG_OUT_LAN_RST_Pin) : (GPIO_DSIG_OUT_LAN_RST_GPIO_Port->BSRRH = (GPIO_DSIG_OUT_LAN_RST_Pin)))

/* USER CODE BEGIN Private defines */
extern char log_string[205];

#if RTT_ENABLED
#define RTT_LOG(...) {\
	sprintf(log_string, "%06d ", HAL_GetTick());\
	snprintf(log_string+7, 180, __VA_ARGS__); \
	strcat(log_string,"\n");\
	SEGGER_RTT_WriteString(0,log_string);\
}
#else 
#define RTT_LOG(...) {}
#endif
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
