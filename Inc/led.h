/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : led.h
  * @brief          : Header for led.c file.
  *                   This file contains the common defines of the application.
	* @author					: Bogdan Fabianski
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 Poznan University of Technology.
  * All rights reserved.</center></h2>
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LED_H
#define __LED_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"
#include "main.h"


void Error_Handler(void);

#define DSIG_RGB_B_BLUE(z)							 				(z ? (GPIO_OUT_RGB_BLUE_GPIO_Port->BSRRL = GPIO_OUT_RGB_BLUE_Pin) : (GPIO_OUT_RGB_BLUE_GPIO_Port->BSRRH = GPIO_OUT_RGB_BLUE_Pin))
#define DSIG_RGB_B_BLUE_TOGGLE									GPIO_OUT_RGB_BLUE_GPIO_Port->ODR^=(GPIO_OUT_RGB_BLUE_Pin)

#define DSIG_RGB_B_GREEN(z)							 				(z ? (GPIO_OUT_RGB_GREEN_GPIO_Port->BSRRL = GPIO_OUT_RGB_GREEN_Pin) : (GPIO_OUT_RGB_GREEN_GPIO_Port->BSRRH = GPIO_OUT_RGB_GREEN_Pin))
#define DSIG_RGB_B_GREEN_TOGGLE								GPIO_OUT_RGB_GREEN_GPIO_Port->ODR^=(GPIO_OUT_RGB_GREEN_Pin)

#define DSIG_RGB_B_RED(z)							 				(z ? (GPIO_OUT_RGB_RED_GPIO_Port->BSRRL = GPIO_OUT_RGB_RED_Pin) : (GPIO_OUT_RGB_RED_GPIO_Port->BSRRH = GPIO_OUT_RGB_RED_Pin))
#define DSIG_RGB_B_RED_TOGGLE								GPIO_OUT_RGB_RED_GPIO_Port->ODR^=(GPIO_OUT_RGB_RED_Pin)


void ledProcess(__IO uint8_t* led_delay);

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT Poznan University of Technology *****END OF FILE****/
