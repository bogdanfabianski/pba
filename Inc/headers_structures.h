/**
  ******************************************************************************
  * @file           : headers_structures.h
  * @brief          : structures of interconn header data for three different interfaces
  *      
  ******************************************************************************
		@copyrights			Poznan University of Technology -- SCADVANCE TEAM C@2019 (Bogdan Fabianski)
  ******************************************************************************
**/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HEADERS_STRUCTURES_H__
#define __HEADERS_STRUCTURES_H__

#include "stm32h7xx_hal.h"



#ifdef __cplusplus
 extern "C" {
#endif
	 

/******* SERIAL/UART DATA FRAME **************/
#define UART_DMA_RX_MAX_LEN 512

typedef struct _spi_serial_data_header_t{
uint8_t interface; //0+1
uint8_t protocol; //1+1
uint8_t hard_error;//2+1
uint8_t soft_error;//3+1
uint16_t dst_add; //4+2
uint16_t src_add; //6+2
uint32_t timestamp_l;//8+4
uint32_t timestamp_h;//12+4
uint16_t data_len; //16+2
uint16_t crc; //18+2
}spi_serial_data_header; //20


#define UART_RX_OVERHEAD_SIZE 20  //20=1+1+2+2+1+1+8+2+2   INT|PROT|DST|SRC|HERR|AERR|TS|DLEN|CRC   (old: SRC|DST|HERR|AERR|LEN|CRC)
#define UART_RX_LEN_OFFSET 16  //20=1+1+2+2+1+1+8+2+2   INT|PROT|DST|SRC|HERR|AERR|TS|DLEN|CRC   (old: SRC|DST|HERR|AERR|LEN|CRC)
#define UART_RX_CRC_OFFSET 18  //20=1+1+2+2+1+1+8+2+2   INT|PROT|DST|SRC|HERR|AERR|TS|DLEN|CRC   (old: SRC|DST|HERR|AERR|LEN|CRC)

/******* ETHERNET DATA FRAME **************/

typedef struct _spi_ethernet_data_header_t{
uint8_t interface; //0+1
uint8_t protocol; //1+1
uint8_t hard_error;//2+1
uint8_t soft_error;//3+1
uint8_t dst_add[6]; //4+6
uint8_t src_add[6]; //10+6
uint16_t eth_type; //16+2
uint16_t dst_port; //18+2
uint16_t src_port; //20+2
uint16_t _rsvd_; //22+2
uint32_t timestamp_l;//24+4
uint32_t timestamp_h;//28+4
uint16_t data_len; //32+2
uint16_t crc; //34+2
}spi_ethernet_data_header; //36


#define ETHERENT_RX_OVERHEAD_SIZE  36   //  6+6+2+2+2+1+1+2+2 INT | PROT| SRC | DST | TYPE | S_PORT | D_PORT | HERR | AERR | LEN | CRC
#define ETHERENT_RX_LEN_OFFSET 32 
#define ETHERENT_RX_CRC_OFFSET 34  

/******* CAN DATA FRAME **************/

typedef struct _spi_can_data_header_t{
uint8_t interface; //0+1
uint8_t protocol; //1+1
uint8_t hard_error;//2+1
uint8_t soft_error;//3+1
uint32_t dst_add; //4+4
uint32_t src_add; //8+4
uint32_t timestamp_l;//12+4
uint32_t timestamp_h;//16+4
uint16_t data_len; //20+2
uint16_t crc; //22+2
}spi_can_data_header; //24


#define CAN_RX_OVERHEAD_SIZE 24   //  6+6+2+2+2+1+1+2+2 INT | PROT | SRC | DST | HERR | AERR | LEN | CRC
#define CAN_RX_LEN_OFFSET 20 
#define CAN_RX_CRC_OFFSET 22  
	 
	 
#ifdef __cplusplus
}
#endif

#endif /* __HEADERS_STRUCTURES_H__ */

/************************ (C) COPYRIGHT Poznan University of Technology *****END OF FILE****/
