/**
  ******************************************************************************
  * @file           : commands.h
  * @brief          : Header for commands.c file.
  *                   definitions for SPI commands 
  ******************************************************************************
					PP SCADVANCE TEAM C@2018 (B.Fabianski)
					created: 30.11.2018
					v1.0
  ******************************************************************************
**/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SPI_COMMANDS_H__
#define __SPI_COMMANDS_H__

#include "main.h"

#define SOFT_RESET_MAGIC_NUMBER 0xAB

enum __commands_type{
	commands_type_critical = 0,
	commands_type_uart_settings,
	commands_type_interface_protocol,
	commands_type_ethernet_settings,
	commands_type_set_timestamp
};

typedef enum __commands_type commands_type;

enum __commands_param_critical{
	commands_param_critical_reserved = 0,
	commands_param_critical_system_reset =1,
};
typedef enum __commands_param_critical commands_param_critical;

void command_process(char* command_string);
void cmd_reinit_uart(void);
void cmd_drive_set_protocol(void);
void cmd_deinit_uart(void);
void cmd_init_uart(void);

uint8_t is_soft_reset(void);
void soft_reset_clear(void);

#endif   //__SPI_COMMANDS_H__
