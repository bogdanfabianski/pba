/**
  ******************************************************************************
  * @file           : inter_prot_parameters.h
  * @brief          : Header for inter_prot_parameters.c file.
  *                   structure defines for interface/protocol selection
  ******************************************************************************
					PP SCADVANCE TEAM C@2019 (B.Fabianski)
					created: 02.03.2019
					v1.0
  ******************************************************************************
**/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __INTER_PROT_PARAMETERS_H__
#define __INTER_PROT_PARAMETERS_H__

#include <stdint.h>
#include <parameters/parameter-enum.h>

typedef enum peripheral_interface_t interface_selection_t;
//{\"d\":\"Interface type\",\"i\":[\"Disabled\",\"ETHERNET\",\"CAN\",\"RS485\",\"RS232\"]}

typedef enum peripheral_protocol protocol_selection_t;

//{\"d\":\"Serial protocol\",\"i\":[\"Raw\",\"Modbus\",\"Profibus\"]}"

typedef struct __ip_parameters {
	  interface_selection_t interface;
	  protocol_selection_t protocol;
} ip_parameters_t;

void ip_init_params(void);

ip_parameters_t* ip_get_param_structure(void);
uint8_t* ip_get_param_structure_serialized(void);
uint32_t ip_get_param_structure_size(void);
void ip_set_param_from_serial_data(uint8_t* serial_data);
void ip_set_params(interface_selection_t interface,	 protocol_selection_t protocol);
protocol_selection_t ip_selected_protocol(void);
interface_selection_t ip_selected_interface(void);

#endif   //__INTER_PROT_PARAMETERS_H__
