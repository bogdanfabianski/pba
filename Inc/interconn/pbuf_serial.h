/**
  ******************************************************************************
  * @file           : 	pbuf_serial.h
  * @brief          : 	header file for pbuf_serial.c
  ******************************************************************************
	@author			:	PP SCADVANCE TEAM C@2019 (B.Fabianski)
	@date			:	created: 17.06.2019
	@version		:	v1.0
	@copyright		: 	Poznan University of Technology (@2019)
  ******************************************************************************
**/

#ifndef __PBUF_SERIAL_H__
#define __PBUF_SERIAL_H__

#include "main.h"
#include "stm32h7xx_hal.h"
#include "crc_routines.h"
#include "error_handling_comm.h"

typedef enum _pbuf_serial_status{
	pbuf_serial_status_free = 0,
	pbuf_serial_status_locked_dma,
	pbuf_serial_status_locked_ready_to_send,
	pbuf_serial_status_locked_sending_via_spi
} pbuf_serial_status_t;

typedef struct _pbuf_serial{
__IO uint8_t* data_pointer;
uint16_t data_len;
pbuf_serial_status_t status;
struct _pbuf_serial* next;
}pbuf_serial_t;

uint8_t pbuf_serial_data_table_init(void);

//stage initialisation of DMA transfer (data buffer pointers assignment)
pbuf_serial_t* pbuf_serial_data_table_lock_for_dma(void);

//on DMA transfer finished (searchng by address saved in DMA)
pbuf_serial_t* pbuf_serial_data_dma_ready(uint8_t* M0AR_val, uint16_t data_len);

//prepare data to send...(designed to use in main.h)
pbuf_serial_t* pbuf_serial_data_list_get_first_to_send(void);

//after the data are sent via SPI clear pointer...
pbuf_serial_t* pbuf_serial_data_list_free_item(void);

pbuf_serial_t* pbuf_serial_data_dma_error(uint8_t* M0AR_val);

pbuf_serial_t* pbuf_serial_can_data_dma_ready(pbuf_serial_t* pbuf_item, uint16_t data_len);
pbuf_serial_t* pbuf_serial_can_data_list_get_first_to_send(void);


uint8_t pbuf_ethernet_data_table_init(void);
pbuf_serial_t* pbuf_ethernet_data_table_lock_for_dma(void);
pbuf_serial_t* pbuf_ethernet_data_dma_ready(uint8_t* M0AR_val, uint16_t data_len);
pbuf_serial_t* pbuf_ethernet_data_dma_error(uint8_t* M0AR_val);
pbuf_serial_t* pbuf_ethernet_data_list_get_first_to_send(void);
pbuf_serial_t* pbuf_ethernet_data_list_free_item(void);
pbuf_serial_t* pbuf_ethernet_data_ready(pbuf_serial_t* pbuf_item, uint16_t data_len);
pbuf_serial_t* pbuf_ethernet_data_ready_IRQ(pbuf_serial_t* pbuf_item, uint16_t data_len);

#endif //__PBUF_SERIAL_H__
