/**
  ******************************************************************************
  * @file           : crc_routines.h
  * @brief          : Header for crc_routines.c file.
  *                   This file contains the common defines for SPI CRC calculation
  ******************************************************************************
											PP SCADVANCE TEAM C@2018 (B.Fabianski)
											created: 27.11.2018
											v1.0
  ******************************************************************************
**/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CRC_ROUTINES_H__
#define __CRC_ROUTINES_H__

#include "main.h"

void MX_CRC_Init(void);
void CRC_AddCRC16ToSpiData(uint8_t* pBuffer, uint16_t BufferLength);

#endif   //__CRC_ROUTINES_H__
