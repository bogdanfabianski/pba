/**
  ******************************************************************************
  * @file           : error_handling_comm.h
  * @brief          : Header for error_handling_comm.c file.
  *                   defines for communication error handling
  ******************************************************************************
											PP SCADVANCE TEAM C@2018 (B.Fabianski)
											created: 31.11.2018
											v1.0
  ******************************************************************************
**/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ERROR_HANDLING_COMM_H__
#define __ERROR_HANDLING_COMM_H__

#include "main.h"


#define IWDG_RELOAD_VALUE 0xFFFF

//measured in HMI thread ticks   
#define ERROR_HANDLING_HBB_PERIOD  50				//in HMI loops delay (set to 100ms*50 = 5s)


uint8_t error_handling_hbb_tick(void);
void error_handling_hbb_reset(void);

#endif   //__ERROR_HANDLING_COMM_H__
