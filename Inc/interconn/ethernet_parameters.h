/**
  ******************************************************************************
  * @file           : inter_prot_parameters.h
  * @brief          : Header for ethernet_parameters.c file.
  *                   structure defines for ethernet params. selection
  ******************************************************************************
					PP SCADVANCE TEAM C@2019 (B.Fabianski)
					created: 11.06.2019
					v1.0
  ******************************************************************************
**/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ETHERNET_PARAMETERS_H__
#define __ETHERNET_PARAMETERS_H__

#include <stdint.h>
#include "parameters/parameter-setup-const.h"
#include "parameters/parameter-enum.h"


typedef enum parameter_ethernet_operation_mode ethernet_operation_mode_t;
/*
parameter_mac_address_t macPB;			
				 parameter_ip_address_t ip;
				 parameter_ip_address_t netmask;
				 parameter_ip_address_t gateway;
				 parameter_select_t operation_mode;
*/

typedef struct __ethernet_parameters {
	  uint8_t macPB[6];
	  uint8_t ip[4];
	  uint8_t netmask[4];
	  uint8_t gateway[4];
	  ethernet_operation_mode_t ethernet_operation_mode;
} ethernet_parameters_t;

void ethernet_init_params(void);

ethernet_parameters_t* ethernet_get_param_structure(void);
uint8_t* ethernet_get_param_structure_serialized(void);
uint32_t ethernet_get_param_structure_size(void);
void ethernet_set_param_from_serial_data(uint8_t* serial_data);
void ethernet_set_params(void);
#endif   //__ETHERNET_PARAMETERS_H__
