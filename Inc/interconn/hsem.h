/**
  ******************************************************************************
  * @file           : hsem.h
  * @brief          : Header for hsem.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
											PP SCADVANCE TEAM C@2019
  ******************************************************************************
**/

#include "main.h"

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HSEM_H__
#define __HSEM_H__


#define HSEM_ID (9U)
#define HSEM_PROCESS_1 12U
#define HSEM_PROCESS_2 13U

//extern uint32_t Semstatus;


#endif //__HSEM_H__

